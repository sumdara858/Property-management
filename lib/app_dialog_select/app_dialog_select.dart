import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';

import '../widget/item_select_dialog/item_select_dialog.dart';

class AppDialog{


  static Future<void> dialogSingleSelect(BuildContext context,{String? title , int currentIndex = -1 , List<Map>? listSelect,ValueChanged<int>? selectedIndex })async{

    Rx<int> indexSelected = 0.obs;

    indexSelected.value = currentIndex;

    return showDialog(context: context, builder: (BuildContext context){
      return AlertDialog(
        scrollable: true,
        contentPadding: const EdgeInsets.all(0),
        insetPadding: const EdgeInsets.symmetric(horizontal: 0),
        actions: [
          TextButton(onPressed: (){
            Get.back();
          }, child: Text("Cancel".tr,style: AppFont.textStyleHeader(fontWeight: FontWeight.w500,fontSize: AppDimension.getSizeTextTitleItemProperty(context)+4,color: AppColor.greyColor),)),
          TextButton(
              onPressed: (){
                if(indexSelected.value!=-1){
                  Get.back();
                  selectedIndex!(indexSelected.value);
                }
              }
              , child: Obx((){

                if(indexSelected.value!=-1){
                  return Text("Select".tr,style: AppFont.textStyleHeader(fontWeight: FontWeight.w500,fontSize: AppDimension.getSizeTextTitleItemProperty(context)+4,color:AppColor.primaryColor),);

                }

                  return Text("Select".tr,style: AppFont.textStyleHeader(fontWeight: FontWeight.w500,fontSize: AppDimension.getSizeTextTitleItemProperty(context)+4,color:AppColor.greyColor),);
                }
              )),
        ],
        title: Text("$title",style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context)-2),),
        content: StatefulBuilder(builder: (BuildContext context, void Function(void Function()) setStateBuilder) {
          return SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
              child: Column(
                children: List.generate(listSelect!.length, (index){
                  return InkWell(
                    onTap: (){
                        setStateBuilder((){
                          indexSelected.value = index;
                        });
                    },
                    child: ItemSelectDialogWidget(
                      title: listSelect[index]["title"],
                      isSelected: indexSelected.value==index,
                    ),
                  );
                }),
              ),
            ),
          );

        },

        ),
      );
      
    });

  }


  static Future<void> loadingDialog(BuildContext context)async{

    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context)
    {
      return AlertDialog(
        scrollable: true,
        contentPadding: const EdgeInsets.all(0),
        insetPadding: const EdgeInsets.symmetric(horizontal: 0),
        content: Container(
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(width: AppDimension.appSpaceVertical,),

              SizedBox(
                  width: AppDimension.getSizeIcon(context),
                  height: AppDimension.getSizeIcon(context),
                  child: const CircularProgressIndicator()
              ),
              const SizedBox(
                width: AppDimension.appSpaceVertical,
              ),
              Text("Loading".tr,style: AppFont.textStyleHeader(fontWeight: FontWeight.w400,fontSize: AppDimension.getSizeTextTitleItemProperty(context)+4)),
              const SizedBox(width: 2,),
              Container(
                width: 4,
                height: 4,
                margin: const EdgeInsets.only(top: AppDimension.appSpaceVertical*1.5),
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                  color: AppColor.blackColor,
                  shape: BoxShape.circle
                ),
              ).animate(
                onComplete: (AnimationController animationController){
                  animationController.repeat();
                }
              ).fadeOut(
                duration: const Duration(milliseconds: 300),delay: const Duration(milliseconds: 300),curve: Curves.easeInOutSine
              ),
              const SizedBox(width: 2,),
              Container(
                width: 4,
                height: 4,
                margin: const EdgeInsets.only(top: AppDimension.appSpaceVertical*1.5),
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                    color: AppColor.blackColor,
                    shape: BoxShape.circle
                ),
              ).animate(
                  onComplete: (AnimationController animationController){
                    animationController.repeat();
                  }
              ).fadeOut(
                 duration: const Duration(milliseconds: 400,),delay: const Duration(milliseconds: 400),curve: Curves.easeInOutSine
              ),
              const SizedBox(width: 2,),
              Container(
                width: 4,
                height: 4,
                margin: const EdgeInsets.only(top: AppDimension.appSpaceVertical*1.5),
                alignment: Alignment.bottomCenter,
                decoration: BoxDecoration(
                    color: AppColor.blackColor,
                    shape: BoxShape.circle
                ),
              ).animate(
                  onComplete: (AnimationController animationController){
                    animationController.repeat();
                  }
              ).fadeOut(
                  duration: const Duration(milliseconds: 500),delay: const Duration(milliseconds: 500),curve: Curves.easeInOutSine
              )
            ],
          ),
        ),
      );

    });


  }


  static Future<void> alertDialog(BuildContext context,{String? message,Color errorColor = Colors.black,GestureTapCallback? onClickOK})async{
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context)
        {
          return CupertinoAlertDialog(
            actions: [
              TextButton(onPressed:onClickOK??(){
                Get.back();
              }, child: Text("Ok".tr,style: AppFont.textStyleTitle(color: AppColor.primaryColor),))
            ],
            content: Container(
              margin: const EdgeInsets.all(AppDimension.appSpaceVertical/2),
              padding: const EdgeInsets.all(AppDimension.appSpaceVertical/2),
              child: Text("$message",style: AppFont.textStyleTitle(color:errorColor,fontWeight: FontWeight.w500,fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context)),),
            ),
          );
        });

  }

}