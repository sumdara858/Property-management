class UserModel{

  String? userName;
  String? userEmail;
  String? phoneNumber;
  String? designation;
  String? institution;
  String? passportNumber;
  String? gender;
  String? occupation;

  UserModel(
  {
    this.userName,
    this.userEmail,
    this.phoneNumber,
    this.designation,
    this.institution,
    this.passportNumber,
    this.gender,
    this.occupation
  }
  );


  UserModel.fromJson(Map json){
    userName = json["userName"].toString();
    userEmail = json["userEmail"].toString();
    phoneNumber = json["phoneNumber"].toString();
    designation = json["designation"].toString();
    institution = json["institution"].toString();
    passportNumber = json["passportNumber"].toString();
    gender = json["gender"].toString();
    occupation = json["occupation"].toString();
  }

}