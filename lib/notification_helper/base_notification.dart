
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'dart:io' show Platform;
class BaseNotification{

  static String action = "";

  static String actionID  ="";

  static bool isFromNotification = false;

  static String appID = "1:573804070879:ios:d0c2d82408ca5cc322666a";

  static void initBaseNotification()async{

    appID = Platform.isAndroid?"1:573804070879:android:b563831b04e8ab8222666a":"1:573804070879:ios:d0c2d82408ca5cc322666a";

    WidgetsFlutterBinding.ensureInitialized();

    await Firebase.initializeApp(
        options: FirebaseOptions(apiKey: "AIzaSyBqIvuqL-bqCoyflUG7NJim-eOfdgXi8zA", appId: appID, messagingSenderId: "573804070879", projectId: "property-management-d35cc")
    );


    FirebaseMessaging firebaseMessaging =  FirebaseMessaging.instance;

    firebaseMessaging.setAutoInitEnabled(true);

    String? tokenPhone = await firebaseMessaging.getToken();

    print("Token Phone : $tokenPhone");

    //request permission notification
    await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );


    AwesomeNotifications().initialize(
        null,
        [            // notification icon
          NotificationChannel(
              channelGroupKey: 'myApp',
              channelKey: 'myApp',
              channelName: 'myApp',
              channelDescription: 'Notification channel for image tests',
              defaultColor: Colors.white,
              ledColor: Colors.white,
              channelShowBadge: true,
              importance: NotificationImportance.High
          )

          //add more notification type with different configuration
        ]
    );
    bool isAllowedNotification = await AwesomeNotifications().isNotificationAllowed();

    if (!isAllowedNotification) {
      //no permission of local notification
      AwesomeNotifications().requestPermissionToSendNotifications();
    }

    //listen event send notification from back-end
   FirebaseMessaging.onMessage.listen((RemoteMessage message) {
     action = message.data["action"]??"";
     actionID = message.data["id"]??"";
     isFromNotification = true;
     AwesomeNotifications().createNotification(
         content: NotificationContent(
           displayOnBackground: true,
           fullScreenIntent: true,
           displayOnForeground: true,//with asset image
           id: 1234,
           channelKey: 'myApp',
           title: '${message.notification?.title.toString()}',
           body: '${message.notification?.body.toString()}',
           bigPicture: message.data["image"]??'assets/images/report.png',
           notificationLayout: NotificationLayout.BigPicture,
         ),
         actionButtons: [
           NotificationActionButton(
             key: "open",
             label: "Open",
           ),

         ]
     );
   });

    //tap on notification when user has open app in background or currently in app
    AwesomeNotifications().actionStream.listen((ReceivedNotification receivedNotification){

    });


  }



}