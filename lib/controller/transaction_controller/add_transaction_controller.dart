
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTransactionController extends GetxController{

  TextEditingController propertyNameController = TextEditingController();

  TextEditingController transactionTypeController = TextEditingController();

  TextEditingController tenantNameController = TextEditingController();

  TextEditingController dateController = TextEditingController();

  TextEditingController dueDateController = TextEditingController();

  TextEditingController paymentTypeController = TextEditingController();

  TextEditingController noteController = TextEditingController();

  int indexProperty = -1;

  List<Map> listProperty = [
    {
      "title":"Green Lake"
    },
    {
      "title":"6Floors building"
    },
    {
      "title":"42Floors building"
    },
    {
      "title":"Condo"
    }
  ];

  int indexTransactionType = -1;
  List<Map> listTransactionType = [
    {
      "title":"Income"
    },
    {
      "title":"Expense"
    },
  ];

  int indexTenants = -1;
  List<Map> listTenants = [
    {
      "title":"Mr Rith"
    },
    {
      "title":"Ms Rothana"
    },
    {
      "title":"Mr Bo"
    },
  ];

  int indexPaymentType = -1;
  List<Map> listPaymentType = [
    {
      "title":"Cash"
    },{
      "title":"Bank Transfer"
    },
  ];
}