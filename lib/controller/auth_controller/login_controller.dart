import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_dialog_select/app_dialog_select.dart';
import 'package:propertymanagement/app_local_database/app_local_database.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/page/main_home_page.dart';

class LoginController extends GetxController{


  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  var isHidePassword = true.obs;


  Map accountLogin = {};


  @override
  void onInit()async{
    super.onInit();
    accountLogin = await AppLocalDataBase.getAccountLogin();
  }


  Future<void> loginUser(BuildContext context)async{

    AppDialog.loadingDialog(context);

    Future.delayed(const Duration(seconds: 3),(){
      Get.back();
      if(emailController.text==accountLogin["userEmail"]&&passwordController.text==accountLogin["password"])
      {
        
        AppLocalDataBase.setUserLogin(true);
        
        Get.offAll(()=>MainHomePage(indexBottomNavigation: 2));
        
      }

      else{
        AppDialog.alertDialog(context,message: "Incorrect email or password".tr,errorColor: AppColor.redColor);
      }

    });

  }


}