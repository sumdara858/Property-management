import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_local_database/app_local_database.dart';
import 'package:propertymanagement/page/main_home_page.dart';

import '../../app_dialog_select/app_dialog_select.dart';

class SignUpController extends GetxController{


  TextEditingController nameController = TextEditingController();

  TextEditingController phoneNumberController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController confirmPasswordController = TextEditingController();


  var isHidePassword = true.obs;

  var isHideConfirmPassword = true.obs;


  Future<void> signUpUser(BuildContext context)async{


    Map userDataJson = {
      "userName":nameController.text,
      "userEmail":emailController.text,
      "phoneNumber":phoneNumberController.text,
      "designation":"N/A",
      "institution":"N/A",
      "passportNumber":"N/A",
      "gender":"N/A",
      "occupation":"N/A"
    };
    
    
    Map userLogin = {
      "userEmail":emailController.text,
      "password":passwordController.text
    };
    
    
    
    AppDialog.loadingDialog(context);
    
    Future.delayed(const Duration(seconds: 3),()async{
      Get.back();
      await  AppLocalDataBase.saveAccountLogin(userLogin);
      await  AppLocalDataBase.saveUserObject(userDataJson);
      Get.offAll(()=>MainHomePage(indexBottomNavigation: 1));
    });
    
    
  }

}