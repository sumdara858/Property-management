import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_dialog_select/app_dialog_select.dart';
import 'package:propertymanagement/app_style/app_color.dart';

import '../../app_local_database/app_local_database.dart';
import '../../model/UserModel.dart';

class ProfileController extends GetxController{


  TextEditingController nameController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  TextEditingController phoneNumberController = TextEditingController();

  TextEditingController designationController = TextEditingController();

  TextEditingController institutionController = TextEditingController();

  TextEditingController occupationController = TextEditingController();

  TextEditingController passportNumberController = TextEditingController();

  TextEditingController genderController = TextEditingController();


  TextEditingController currentPasswordController = TextEditingController();

  TextEditingController newPasswordController = TextEditingController();

  TextEditingController confirmPasswordController = TextEditingController();

  var userModel = UserModel().obs;

  var isLoading = false.obs;

  var isShowCurrentPassword = false.obs;

  var isShowPassword = false.obs;

  var isShowConfirmPassword = false.obs;

  Map userAccount = {};

  @override
  void onInit()async{
    super.onInit();
    isLoading.value = true;
    Future.delayed(const Duration(seconds: 3),()async{
      userAccount = await AppLocalDataBase.getAccountLogin();
      userModel.value = UserModel.fromJson(await AppLocalDataBase.getUserObject());
      isLoading.value = false;
      nameController.text = userModel.value.userName.toString();
      emailController.text = userModel.value.userEmail.toString();
      phoneNumberController.text = userModel.value.phoneNumber.toString();
      designationController.text = userModel.value.designation.toString();
      institutionController.text = userModel.value.institution.toString();
      occupationController.text  = userModel.value.occupation.toString();
      passportNumberController.text        = userModel.value.passportNumber.toString();
      genderController.text = userModel.value.gender.toString();
    });
  }


  Future<void> editProfileUser(BuildContext context)async{

    Map userJson = {
      "userName":nameController.text,
      "userEmail":emailController.text,
      "phoneNumber":phoneNumberController.text,
      "designation":designationController.text,
      "institution":institutionController.text,
      "passportNumber":passportNumberController.text,
      "gender":genderController.text,
      "occupation":occupationController.text
    };
    AppDialog.loadingDialog(context);

    Future.delayed(const Duration(seconds: 3),()async{
     Get.back();
     bool isSuccess =   await AppLocalDataBase.saveUserObject(userJson);
     if(isSuccess){
       Get.back(result: true);
     }
    });

  }

  Future<void> resetPassword(BuildContext context)async{


    Map accountLogin = {
      "userEmail":userAccount["userEmail"],
      "password":newPasswordController.text
    };
    AppDialog.loadingDialog(context);
    Future.delayed(const Duration(seconds: 3),(){
      Get.back();
      if(currentPasswordController.text!=userAccount["password"]){
        AppDialog.alertDialog(context,message: "Incorrect current password!".tr,errorColor: AppColor.redColor);
        return;
      }
      AppDialog.alertDialog(context,message: "Reset password successfully!".tr,errorColor: AppColor.primaryColor);
      AppLocalDataBase.saveAccountLogin(accountLogin);

    });
  }


}