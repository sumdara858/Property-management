import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddPropertyController extends GetxController{


  TextEditingController propertyNameController = TextEditingController();

  TextEditingController propertyTypeController = TextEditingController();

  TextEditingController categoryController     = TextEditingController();

  TextEditingController addressController      = TextEditingController();

  TextEditingController countryController      = TextEditingController();

  TextEditingController cityController         = TextEditingController();

  TextEditingController postalCodeController   = TextEditingController();


  int indexListCategory = -1;

  int indexPropertyType = -1;

  List<Map> listCategoryType = [
    {
      "title":"Apartment"
    },
    {
      "title":"Building"
    },
    {
      "title":"Office"
    },
    {
      "title":"Room"
    },
    {
      "title":"Flat"
    },
    {
      "title":"Land"
    }
  ];

  List<Map> listPropertyType = [
    {
      "title":"Residential"
    },
    {
      "title":"Commercial"
    },
  ];

}