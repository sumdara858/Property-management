import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddDocumentController extends GetxController{

  TextEditingController tenantsNameController = TextEditingController();

  TextEditingController fileNameController = TextEditingController();

  int indexTenants = -1;

  List<Map> listTenants = [
    {
      "title":"Mr Rith"
    },
    {
      "title":"Ms Rothana"
    },
    {
      "title":"Mr Bo"
    },
  ];

  String fileName = "";
  String fileSize = "";
  String fileExtension = "";

}