import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class TenantController extends GetxController{
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController moveInDateController = TextEditingController();
  TextEditingController moveOutDateController = TextEditingController();
  TextEditingController advanceAmountController = TextEditingController();
  TextEditingController rentAmountController = TextEditingController();

}