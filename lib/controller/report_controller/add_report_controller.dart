import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddReportController extends GetxController{


  TextEditingController propertyNameController = TextEditingController();

  TextEditingController tenantNameController   = TextEditingController();

  TextEditingController monthController = TextEditingController();

  int indexPropertyName = -1;
  List<Map> listProperty = [
    {
      "title":"Green Lake"
    },
    {
      "title":"6Floors building"
    },
    {
      "title":"42Floors building"
    },
    {
      "title":"Condo"
    }
  ];

  int indexTenants = -1;
  List<Map> listTenants = [
    {
      "title":"Mr Rith"
    },
    {
      "title":"Ms Rothana"
    },
    {
      "title":"Mr Bo"
    },
  ];

  int indexMonth = -1;
  List<Map> listMonthly = [
    {
      "title":"Last 12 Month"
    },
    {
      "title":"Last 6 Month"
    },
    {
      "title":"Last 3 Month"
    },
  ];



}