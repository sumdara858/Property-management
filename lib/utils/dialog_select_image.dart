import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';

Future<void> dialogSelectImage(BuildContext context,{required GestureTapCallback onSelectGallery ,required GestureTapCallback onSelectCamera }) async {
  return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(
            "Please choose media to select".tr,
            style: AppFont.textStyleTitle(
                fontSize:
                    AppDimension.getSizeTextInformationProfilePage(context),
                fontWeight: FontWeight.w600),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton(
                  onPressed: onSelectGallery,
                  child: Row(
                    children: [
                      const Icon(FontAwesomeIcons.image),
                      const SizedBox(width: AppDimension.appSpaceVertical,),
                      Text("From Gallery".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w500,color: AppColor.whiteColor),)
                    ],
                  )),

              const SizedBox(
                height: AppDimension.appSpaceVertical/2,
              ),

              ElevatedButton(
                  onPressed: onSelectCamera,
                  child: Row(
                    children: [
                      const Icon(FontAwesomeIcons.camera),
                      const SizedBox(width: AppDimension.appSpaceVertical,),
                      Text("From Camera".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w500,color: AppColor.whiteColor),)
                    ],
                  )),

              const SizedBox(
                height: AppDimension.appSpaceVertical,
              ),
            ],
          ),
        );
      });
}
