



import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_font.dart';

import 'internet_connection.dart';

Future<bool> dialogNoInternetConnection(BuildContext context)async{
 bool isHaveInternet = false;
 await showCupertinoDialog<bool>(
    context: context,
    builder: (context){
     return CupertinoAlertDialog(
      title: Text("No internet connection".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w600,fontSize: 16,),),
      content: Text("Please check your internet connection".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w400,fontSize: 14,)),
      actions: [
         TextButton(onPressed: ()async{

           bool checkInternet = await InternetConnection.checkInternet();

           if(checkInternet){
             isHaveInternet = true;
             Get.back();
           }

         }, child: Text("Ok".tr,style: AppFont.textStyleHeader(fontSize: 16),)
         )
      ],
      );
    },
  );
  return isHaveInternet;
}