import 'dart:async';

import 'package:internet_connection_checker/internet_connection_checker.dart';

class InternetConnection{

 static Future<bool> checkInternet()async{
    return  await InternetConnectionChecker().hasConnection;;
  }

}