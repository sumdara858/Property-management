import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
class AppLocalDataBase{


  static const String userObject = "userObject";

  static const String isLogin = "userLogin";

  static const String accountLogin = "accountLogin";


 static Future<Map> getUserObject()async{

    var sharePreference = await SharedPreferences.getInstance();

    Map<String,String> user = {
      "userName":"MR A",
      "userEmail":"MrA@gmail.com",
      "phoneNumber":"099456888",
      "designation":"LandLord",
      "institution":"ABC company",
      "passportNumber":"123AVC",
      "gender":"Male",
      "occupation":"Businessman"
    };
    
    return jsonDecode(sharePreference.getString(userObject)??jsonEncode(user));
  }


  static Future<bool> saveUserObject(Map userJson)async{

    var sharePreference = await SharedPreferences.getInstance();

    await sharePreference.setString(userObject, jsonEncode(userJson));

    return true;
  }


  static Future<bool> isUserLogin()async{

   var sharePreference = await SharedPreferences.getInstance();

   return sharePreference.getBool(isLogin)??false;
  }

  static Future<void> setUserLogin(bool setUserLogin)async{

   var sharePreference = await SharedPreferences.getInstance();

   sharePreference.setBool(isLogin, setUserLogin);

  }


  static Future<Map> getAccountLogin()async{
   var sharePreference = await SharedPreferences.getInstance();
   Map<String,String> demoAccount = {
     "userEmail":"MrA@gmail.com",
     "password":"12345678"
   };
   return jsonDecode(sharePreference.getString(accountLogin)??jsonEncode(demoAccount));
  }

  static Future<void> saveAccountLogin(Map setAccountLogin)async{
    var sharePreference = await SharedPreferences.getInstance();
    sharePreference.setString(accountLogin, jsonEncode(setAccountLogin));

  }
}