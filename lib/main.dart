import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';
import 'package:propertymanagement/splash_screen.dart';

import 'notification_helper/base_notification.dart';

void main() async{




    BaseNotification.initBaseNotification();

    runApp(MyApp());


}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Property Management',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        tabBarTheme: TabBarTheme(
          indicatorColor: AppColor.primaryColor,
          indicatorSize: TabBarIndicatorSize.tab,
          dividerColor: AppColor.whiteColor.withOpacity(0),
        ),
        splashColor: AppColor.primaryColor.withOpacity(0.1),
        primaryColor: AppColor.primaryColor,
        appBarTheme: AppBarTheme(
          backgroundColor: AppColor.primaryColor,
          foregroundColor: AppColor.whiteColor
        ),
        datePickerTheme: DatePickerThemeData(
          backgroundColor: AppColor.whiteColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
          )
        ),
        dialogTheme: DialogTheme(
          backgroundColor: AppColor.whiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
          )
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(AppColor.primaryColor.withOpacity(0.1)),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0)
            ))
          )
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            overlayColor: MaterialStateProperty.all(AppColor.greyColor),
            backgroundColor: MaterialStateProperty.all(AppColor.primaryColor),
            foregroundColor: MaterialStateProperty.all(AppColor.whiteColor),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
            )
            )
          )
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          elevation: AppDimension.defaultElevation,
          unselectedLabelStyle: AppFont.textStyleSubTitle(
            fontSize: AppDimension.getTextSizeBottomNavigationUnSelected(context),
            fontWeight: FontWeight.w400
          ),
          selectedLabelStyle: AppFont.textStyleSubTitle(
              fontSize: AppDimension.getTextSizeBottomNavigationSelected(context),
              fontWeight: FontWeight.w600,
            color: AppColor.primaryColor
          ),
        ),
        scaffoldBackgroundColor: AppColor.backGroundScaffold,
        colorScheme: ColorScheme.fromSeed(seedColor: AppColor.primaryColor),
        useMaterial3: true,
      ),
      home: SplashScreen(),
    );
  }
}

