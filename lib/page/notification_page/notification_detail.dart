import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';

class NotificationDetailPage extends StatefulWidget {
  const NotificationDetailPage({super.key});

  @override
  State<NotificationDetailPage> createState() => _NotificationDetailPageState();
}

class _NotificationDetailPageState extends State<NotificationDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notification Details".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context),color: AppColor.whiteColor),),
      ),

      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: AppDimension.appSpaceVertical/2,),

              Text("Item Delivered".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getTextSizeHeaderItemNotification(context),fontWeight: FontWeight.w600),),

              const SizedBox(height: AppDimension.appSpaceVertical/2,),

              Text("04 sep , 2023 12:54 PM".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w600,fontSize: AppDimension.getTextSizeTimeItemNotification(context),color: AppColor.primaryColor),),

              const SizedBox(height: AppDimension.appSpaceVertical,),

              Text("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w400,fontSize: AppDimension.getTextSizeTimeItemNotification(context),color: AppColor.blackColor),),




            ],
          ),
        ),
      ),
    );
  }
}
