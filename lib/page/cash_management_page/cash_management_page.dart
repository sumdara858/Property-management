import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:propertymanagement/app_string/string_format.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../../widget/item_dashboard/item_transaction.dart';
import '../transaction_page/transaction_detail_page.dart';

class CashManagementPage extends StatefulWidget {


  @override
  State<CashManagementPage> createState() => _CashManagementPageState();
}

class _CashManagementPageState extends State<CashManagementPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Cash management".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(AppDimension.appSpaceVertical*2),
                      decoration: BoxDecoration(
                          color: AppColor.primaryColor,
                          borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                      ),
                      child: Column(
                        children: [
                          Text("Balance".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500),),
                          const SizedBox(height: AppDimension.appSpaceVertical/3,),
                          Text(StringFormat.formatCurrency(1000),style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(width: AppDimension.appSpaceVertical,),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(AppDimension.appSpaceVertical*2),
                      decoration: BoxDecoration(
                          color: AppColor.blueColor,
                          borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                      ),
                      child: Column(
                        children: [
                          Text("Income".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500),),
                          const SizedBox(height: AppDimension.appSpaceVertical/3,),
                          Text(StringFormat.formatCurrency(2500),style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(width: AppDimension.appSpaceVertical,),

                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(AppDimension.appSpaceVertical*2),
                      decoration: BoxDecoration(
                          color: AppColor.orangeColor,
                          borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                      ),
                      child: Column(
                        children: [
                          Text("Expense".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500),),
                          const SizedBox(height: AppDimension.appSpaceVertical/3,),
                          Text(StringFormat.formatCurrency(500),style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                        ],
                      ),
                    ),
                  )
                ],
              ),

              const SizedBox(height: AppDimension.appSpaceVertical,),

              Text("History".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context),fontWeight: FontWeight.w600),),

              Column(
                children: List.generate(10, (index) {
                  if (index == 9) {
                    return Container(
                        margin: const EdgeInsets.only(
                            bottom: AppDimension.appSpaceVertical),
                        child: ItemTransaction(
                          onTap: () {
                            Get.to(()=>TransactionDetailPage());
                          },
                          onTapMenu: () {},
                        ));
                  }
                  return ItemTransaction(
                    onTap: () {
                      Get.to(()=>TransactionDetailPage());
                    },
                    onTapMenu: () {},
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
