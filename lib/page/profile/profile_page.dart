import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_font.dart';
import 'package:propertymanagement/controller/profile_controller/profile_controller.dart';
import 'package:propertymanagement/page/profile/edit_profile.dart';

import '../../app_style/app_dimension.dart';
import '../../widget/skeleton_container_widget.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  ProfileController profileController = Get.put(ProfileController());


  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin:  const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical/2),
          child:  Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 10,),

              //*user profile
              const SizedBox(
                height: AppDimension.profileUserSize/1.5,
                width: AppDimension.profileUserSize/1.5,
                child: CircleAvatar(
                  backgroundImage: NetworkImage('https://picsum.photos/seed/picsum/200/300'),
                ),
              ),

              
              const SizedBox(height: AppDimension.appSpaceVertical,),
              //*name
              Obx(
                (){
                  if(profileController.isLoading.value){
                    return SkeletonContainerWidget(

                    );
                  }

                  return Text("${profileController.userModel.value.userName}",style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+2,fontWeight: FontWeight.w500),);
                }
              ),
              //*email
              Obx(() {
                if(profileController.isLoading.value){
                  return SkeletonContainerWidget(
                    width: 200,
                  );
                }
                  return Text("${profileController.userModel.value.userEmail}",style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+2,fontWeight: FontWeight.w400,color: AppColor.greyColor),);
                }
              ),

               const SizedBox(height: AppDimension.appSpaceVertical*2,),

              Container(
                padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                  color: AppColor.whiteColor,
                  boxShadow: kElevationToShadow[1]
                ),
                child: Column(
                  children: [
                    
                    //*phone
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                        color: AppColor.grey246Color
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Phone".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(
                                  (){
                                if(profileController.isLoading.value){
                                  return SkeletonContainerWidget(
                                    width: MediaQuery.of(context).size.width/2,
                                  );
                                }

                                return Text("${profileController.userModel.value.phoneNumber}".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                              }
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    //*occupation
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Occupation".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(
                                  (){
                              if(profileController.isLoading.value){
                                return SkeletonContainerWidget(
                                  width: MediaQuery.of(context).size.width/2,
                                );
                              }

                              return Text("${profileController.userModel.value.occupation}",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                            }
                          )),
                        ],
                      ),
                    ),

                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    //*Designation
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                          color: AppColor.grey246Color
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Designation".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(() {
                            if(profileController.isLoading.value){
                              return SkeletonContainerWidget(
                                width: MediaQuery.of(context).size.width/2,
                              );
                            }

                            return Text("${profileController.userModel.value.designation}".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                          })),
                        ],
                      ),
                    ),
                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    //*Institution
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Institution".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(
                                  (){
                                if(profileController.isLoading.value){
                                  return SkeletonContainerWidget(
                                    width: MediaQuery.of(context).size.width/2,
                                  );
                                }

                                return Text("${profileController.userModel.value.institution}".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                              }
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    //*Passport No
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                          color: AppColor.grey246Color
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Passport No".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(
                                  (){
                                if(profileController.isLoading.value){
                                  return SkeletonContainerWidget(
                                    width: MediaQuery.of(context).size.width/2,
                                  );
                                }

                                return Text("${profileController.userModel.value.passportNumber}".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                              }
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    //*Passport No
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: AppDimension.appSpaceVertical,vertical: AppDimension.appSpaceVertical),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Text("Gender".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),)),

                          Expanded(child: Obx(
                                  (){
                                if(profileController.isLoading.value){
                                  return SkeletonContainerWidget(
                                    width: MediaQuery.of(context).size.width/2,
                                  );
                                }

                                return Text("${profileController.userModel.value.gender}".tr,style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)),textAlign: TextAlign.end,);
                              }
                          )),
                        ],
                      ),
                    ),
                    const SizedBox(height: AppDimension.appSpaceVertical,),
                  ],
                ),
              ),

              Container(
                padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                child: ElevatedButton(
                    onPressed: ()async{
                       bool? isUpdateProfile = await Get.to(()=>EditProfilePage());
                       if(isUpdateProfile??false){
                         profileController.onInit();
                       }

                    },
                    child: Container(
                      padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Text("Edit".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context),color: AppColor.whiteColor),),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
