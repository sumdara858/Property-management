import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_string/string_format.dart';
import 'package:propertymanagement/controller/profile_controller/profile_controller.dart';
import 'package:propertymanagement/widget/skeleton_container_widget.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';

class EditProfilePage extends StatefulWidget {
  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> with TickerProviderStateMixin{


  late TabController tabController;

  ProfileController profileController = Get.put(ProfileController());

  final formBasicKey = GlobalKey<FormState>();

  final formResetPasswordKey = GlobalKey<FormState>();

  int indexTab = 0;


  @override
  void initState(){
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(() {
      setState(() {
       indexTab =  tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Edit Profile".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //*user profile
              const SizedBox(
                height: AppDimension.profileUserSize / 1.5,
                width: AppDimension.profileUserSize / 1.5,
                child: CircleAvatar(
                  backgroundImage:
                      NetworkImage('https://picsum.photos/seed/picsum/200/300'),
                ),
              ),

              const SizedBox(
                height: AppDimension.appSpaceVertical,
              ),
              //*name
              Obx(() {
                if(profileController.isLoading.value){
                  return SkeletonContainerWidget();
                }
                return Text(
                    "${profileController.userModel.value.userName}",
                    style: AppFont.textStyleHeader(
                        fontSize: AppDimension.getSizeTextInformationProfilePage(
                                context) +
                            2,
                        fontWeight: FontWeight.w500),
                  );
                }
              ),

              //*email
              Obx(() {
                if(profileController.isLoading.value){
                  return SkeletonContainerWidget(
                    width: 200,
                  );
                }
                  return Text(
                    "${profileController.userModel.value.userEmail}",
                    style: AppFont.textStyleHeader(
                        fontSize: AppDimension.getSizeTextInformationProfilePage(
                                context) +
                            2,
                        fontWeight: FontWeight.w400,
                        color: AppColor.greyColor),
                  );
                }
              ),


              TabBar(tabs: [
                Text(
                  "Basic Info",
                  style: AppFont.textStyleHeader(
                      fontSize: AppDimension.getSizeTextInformationProfilePage(
                          context) +
                          2,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  "Reset Password",
                  style: AppFont.textStyleHeader(
                      fontSize: AppDimension.getSizeTextInformationProfilePage(
                          context) +
                          2,
                      fontWeight: FontWeight.w500),
                ),
              ],controller: tabController,),

              const SizedBox(
                height: AppDimension.appSpaceVertical * 2,
              ),


              Builder(builder: (context){

                if(indexTab==1){
                  return formResetPassword(context);
                }

                return formEditProfile(context);
              })

            ],
          ),
        ),
      ),
    );
  }

  Widget formEditProfile(BuildContext context){
    return Container(
      decoration: BoxDecoration(
        color: AppColor.whiteColor,
        boxShadow: kElevationToShadow[2],
        borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
      ),
      padding: const EdgeInsets.all(AppDimension.smallPadding),
      child: Form(
        key: formBasicKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.nameController,
              validator: (value){
                if(value!.isEmpty){
                  return "*Required";
                }
                return null;
              },
              decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.person_outline),
                  labelText: "Name".tr,
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.emailController,
              keyboardType: TextInputType.emailAddress,
              validator: StringFormat.validateEmail,
              decoration: InputDecoration(
                  labelText: "Email".tr,
                  prefixIcon: const Icon(Icons.email_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.phoneNumberController,
              keyboardType: TextInputType.phone,
              validator: StringFormat.validatePhoneNumber,
              decoration: InputDecoration(
                  labelText: "Phone number".tr,
                  prefixIcon: const Icon(Icons.phone_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.designationController,
              decoration: InputDecoration(
                  labelText: "Designation".tr,
                  prefixIcon: const Icon(Icons.description),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.institutionController,
              decoration: InputDecoration(
                  labelText: "Institution".tr,
                  prefixIcon: const Icon(Icons.business_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.occupationController,
              decoration: InputDecoration(
                  labelText: "Occupation".tr,
                  prefixIcon: const Icon(Icons.business_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.passportNumberController,
              decoration: InputDecoration(
                  labelText: "PassPort number".tr,
                  prefixIcon: const Icon(Icons.note_alt_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.genderController,
              decoration: InputDecoration(
                  labelText: "Gender".tr,
                  prefixIcon: const Icon(Icons.male_outlined),
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            Container(
              padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
              child: ElevatedButton(
                  onPressed: (){
                    if(formBasicKey.currentState!.validate()){
                        profileController.editProfileUser(context);
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text("Save".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context),color: AppColor.whiteColor),),
                  )),
            ),

          ],
        ),
      ),
    );
  }


  Widget formResetPassword(BuildContext context){
    return Container(
      decoration: BoxDecoration(
          color: AppColor.whiteColor,
          boxShadow: kElevationToShadow[2],
          borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
      ),
      padding: const EdgeInsets.all(AppDimension.smallPadding),
      child: Form(
        key: formResetPasswordKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.currentPasswordController,
              validator: StringFormat.validatePassword,
              obscureText: !profileController.isShowCurrentPassword.value,
              decoration: InputDecoration(
                  labelText: "Current Password".tr,
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  prefixIcon: const Icon(Icons.password_outlined),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        profileController.isShowCurrentPassword.value = !profileController.isShowCurrentPassword.value;
                      });
                    },
                    child: Obx((){

                      if(profileController.isShowCurrentPassword.value){
                        return const Icon(FontAwesomeIcons.eye);
                      }

                      return const Icon(FontAwesomeIcons.eyeSlash);
                    }
                    ),
                  ),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.newPasswordController,
              validator: StringFormat.validatePassword,
              obscureText: !profileController.isShowPassword.value,
              decoration: InputDecoration(
                  labelText: "New Password".tr,
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  prefixIcon: const Icon(Icons.password_outlined),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        profileController.isShowPassword.value = !profileController.isShowPassword.value;
                      });
                    },
                    child: Obx((){

                      if(profileController.isShowPassword.value){
                        return const Icon(FontAwesomeIcons.eye);
                      }

                      return const Icon(FontAwesomeIcons.eyeSlash);
                    }
                    ),
                  ),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),

            const SizedBox(
              height: AppDimension.appSpaceVertical,
            ),
            TextFormField(
              controller: profileController.confirmPasswordController,
              validator: (value){
                return StringFormat.validateConfirmPassword(profileController.newPasswordController.text, value);
              },
              obscureText: !profileController.isShowConfirmPassword.value,
              decoration: InputDecoration(
                  labelText: "Confirm Password".tr,
                  labelStyle: AppFont.hintTextFiledStyle(),
                  border:  const OutlineInputBorder(),
                  prefixIcon: const Icon(Icons.password_outlined),
                  suffixIcon: InkWell(
                    onTap: (){
                      setState(() {
                        profileController.isShowConfirmPassword.value = !profileController.isShowConfirmPassword.value;
                      });
                    },
                    child: Obx((){

                      if(profileController.isShowConfirmPassword.value){
                        return const Icon(FontAwesomeIcons.eye);
                      }

                      return const Icon(FontAwesomeIcons.eyeSlash);
                    }
                    ),
                  ),
                  contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
              ),
            ),


            Container(
              padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
              child: ElevatedButton(
                  onPressed: (){
                    if(formResetPasswordKey.currentState!.validate()){
                      profileController.resetPassword(context);
                    }
                  },
                  child: Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text("Save".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context),color: AppColor.whiteColor),),
                  )),
            ),

          ],
        ),
      ),
    );
  }
}
