import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../pdf_file_page.dart';

class TransactionDetailPage extends StatefulWidget {


  @override
  State<TransactionDetailPage> createState() => _TransactionDetailPageState();
}

class _TransactionDetailPageState extends State<TransactionDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Transaction Details".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),

      body: SingleChildScrollView(
        child: Container(
          margin:  const EdgeInsets.symmetric(horizontal:AppDimension.appSpaceVertical*2,vertical: AppDimension.appSpaceVertical),
          child:  Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              const SizedBox(height: AppDimension.appSpaceVertical,),

              Text("Stone brook Place".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context)-2,fontWeight: FontWeight.w500,decoration: TextDecoration.underline),),

              const SizedBox(height: AppDimension.appSpaceVertical/2,),

              Text("This is a note".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getTextSizeHeaderItemNotification(context),fontWeight: FontWeight.w400,color: AppColor.greyColor),),

              const SizedBox(height: AppDimension.appSpaceVertical,),

              //*property name
              Container(
                padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${"Property name".tr} : ",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),fontWeight: FontWeight.w500),),

                    const SizedBox(width: AppDimension.appSpaceVertical,),

                    Expanded(flex:1,child: Text("Green View Terrace",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.greyColor),textAlign: TextAlign.start,)),
                  ],
                ),
              ),



              //*tenants name
              Container(
                padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${"Tenants name".tr} : ",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),fontWeight: FontWeight.w500),),

                    const SizedBox(width: AppDimension.appSpaceVertical,),

                    Expanded(flex:1,child: Text("Mr B",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.greyColor),textAlign: TextAlign.start,)),
                  ],
                ),
              ),



              //*payment date
              Container(
                padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${"Payment data".tr} : ",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),fontWeight: FontWeight.w500),),

                    const SizedBox(width: AppDimension.appSpaceVertical,),

                    Expanded(flex:1,child: Text("16 Aug,2023",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.greyColor),textAlign: TextAlign.start,)),
                  ],
                ),
              ),

              //*payment type
              Container(
                padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${"Payment type".tr} : ",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),fontWeight: FontWeight.w500),),

                    const SizedBox(width: AppDimension.appSpaceVertical,),

                    Expanded(flex:1,child: Text("Cash",style: AppFont.textStyleSubTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.greyColor),textAlign: TextAlign.start,)),
                  ],
                ),
              ),


              const SizedBox(height: AppDimension.appSpaceVertical,),

              Text("File Attachment".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context)-2,fontWeight: FontWeight.w500,decoration: TextDecoration.underline),),

              const SizedBox(height: AppDimension.appSpaceVertical,),

              InkWell(
                onTap: (){
                  Get.to(()=>PdfFilePage(urlPdf:'https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf'));
                },
                child: Container(
                  padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                  decoration: BoxDecoration(
                    color: AppColor.whiteColor,
                    borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Image.asset("assets/icons/pdf.png",width: AppDimension.getSizeIcon(context)+8,height: AppDimension.getSizeIcon(context)+8,),

                      const SizedBox(width: AppDimension.appSpaceVertical,),

                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Note.pdf",style: AppFont.textStyleTitle(fontSize: AppDimension.getTextSizeHeaderItemNotification(context),fontWeight: FontWeight.w500),),
                            Text("7 days ago",style: AppFont.textStyleTitle(fontSize: AppDimension.getTextSizeTimeItemNotification(context),fontWeight: FontWeight.w400,color: AppColor.greyColor),),
                          ],
                        ),
                      )
                      ,
                      const Spacer(),

                      Text("17 MB",style: AppFont.textStyleTitle(fontSize: AppDimension.getTextSizeTimeItemNotification(context),fontWeight: FontWeight.w400,color: AppColor.greyColor),),


                    ],
                  ),
                ),
              )
              
            ],
          ),
        ),
      ),
    );
  }
}
