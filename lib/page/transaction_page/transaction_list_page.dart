import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/page/transaction_page/transaction_detail_page.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../../widget/item_dashboard/item_transaction.dart';
import '../tenants_page/add_tenant_page.dart';
import 'add_transaction_page.dart';

class TransactionListPage extends StatefulWidget {

  @override
  State<TransactionListPage> createState() => _TransactionListPageState();
}

class _TransactionListPageState extends State<TransactionListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CircleAvatar(
        radius:MediaQuery.of(context).size.width>720?AppDimension.getSizeIcon(context)/1.5:AppDimension.getSizeIcon(context),
        backgroundColor: Theme.of(context).primaryColor,
        child: IconButton(
            onPressed: ()async{
              Get.to(()=>AddTransactionPage());
            },
            icon: Icon(Icons.add,color: AppColor.whiteColor,size: AppDimension.getSizeIcon(context))
        ),
      ).animate().slideX(begin: -2.5,end: 0),

      appBar: AppBar(
        title: Text(
          "Transaction".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),
      
      body: Container(
        margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                      width:MediaQuery.of(context).size.width>720?MediaQuery.of(context).size.width*0.88:MediaQuery.of(context).size.width*0.8,
                      child: TextField(
                        decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.search),
                            filled: true,
                            fillColor: AppColor.whiteColor,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                                borderSide: BorderSide(
                                    color: AppColor.greyColor,
                                    width: 0.5
                                )
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:  BorderRadius.circular(AppDimension.defaultRadius),
                                borderSide: BorderSide(
                                    color: AppColor.greyColor,
                                    width: 0.5
                                )
                            ),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical)
                        ),

                      )
                  ),
                  const Spacer(),
                  Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                    decoration: BoxDecoration(
                        borderRadius:BorderRadius.circular(AppDimension.defaultRadius),
                        color: AppColor.whiteColor
                    ),
                    child: InkWell(
                      onTap: (){
                      },
                      child: Icon(FontAwesomeIcons.barsProgress,color: AppColor.primaryColor,size:MediaQuery.of(context).size.width>720? AppDimension.getSizeIcon(context)-16:AppDimension.getSizeIcon(context),),
                    ),
                  ),

                  const SizedBox(width: AppDimension.appSpaceVertical/2,),
                ],
              ),
              const SizedBox(height: AppDimension.appSpaceVertical,),
              Text("Transaction History".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context),fontWeight: FontWeight.w600),),
              const SizedBox(height: AppDimension.appSpaceVertical,),
              SizedBox(
                height: MediaQuery.of(context).size.height*0.75-5,
                child: SingleChildScrollView(
                  child: Column(
                    children: List.generate(10, (index) {
                      if (index == 9) {
                        return Container(
                            margin: const EdgeInsets.only(
                                bottom: AppDimension.appSpaceVertical),
                            child: ItemTransaction(
                              onTap: () {
                                Get.to(()=>TransactionDetailPage());
                              },
                              onTapMenu: () {},
                            ));
                      }
                      return ItemTransaction(
                        onTap: () {
                          Get.to(()=>TransactionDetailPage());
                        },
                        onTapMenu: () {},
                      );
                    }),
                  ),
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}

