import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:propertymanagement/app_dialog_select/app_dialog_select.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'dart:io';
import '../../app_style/app_color.dart';
import '../../app_style/app_font.dart';
import '../../controller/transaction_controller/add_transaction_controller.dart';
import '../../helper/select_file_helper.dart';
import '../../utils/dialog_select_image.dart';

class AddTransactionPage extends StatefulWidget {


  @override
  State<AddTransactionPage> createState() => _AddTransactionPageState();
}

class _AddTransactionPageState extends State<AddTransactionPage> {

  AddTransactionController addTransactionController = Get.put(AddTransactionController());
  final keyForm = GlobalKey<FormState>();
  File? fileImage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add Transaction".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),

      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  //select image
                  if (fileImage == null) {
                    dialogSelectImage(
                        context,
                        onSelectCamera: () async {
                          SelectFileHelper.getImage(imageSource: ImageSource.camera).then((value){
                            setState(() {
                              fileImage = value;
                              Get.back();
                            });
                          });
                        },
                        onSelectGallery: () async {
                          SelectFileHelper.getImage(imageSource:ImageSource.gallery).then((value){
                            setState(() {
                              fileImage = value;
                              Get.back();
                            });
                          });
                        }
                    );
                  }
                },
                child: Container(
                  width: AppDimension.getSizeContainerPicture(context),
                  height: AppDimension.getSizeContainerPicture(context),
                  decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(AppDimension.defaultRadius),
                      border:
                      Border.all(color: AppColor.primaryColor, width: 1.5)),
                  child: Builder(
                    builder: (context) {
                      if (fileImage == null) {
                        return Icon(
                          Icons.add,
                          color: AppColor.primaryColor,
                          size: AppDimension.getSizeIcon(context),
                        );
                      }
                      return Image.file(
                        fileImage!,
                        fit: BoxFit.cover,
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(height: AppDimension.appSpaceVertical,),

              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                decoration: BoxDecoration(
                    color: AppColor.whiteColor,
                    borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                    boxShadow: kElevationToShadow[2]
                ),

                child: Form(
                  key: keyForm,
                  child: Column(
                    children: [
                      Text("Information Transaction".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w600,fontSize: AppDimension.getTextSizeHeaderItemNotification(context)),),
                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.propertyNameController,
                        keyboardType: TextInputType.none,
                        onTap: (){

                          AppDialog.dialogSingleSelect(
                              context,
                              currentIndex: addTransactionController.indexProperty,
                              listSelect: addTransactionController.listProperty,
                              title: "Please select property name".tr,
                              selectedIndex: (int index){
                            addTransactionController.indexProperty = index;

                            if(addTransactionController.indexProperty!=-1){
                              addTransactionController.propertyNameController.text = addTransactionController.listProperty[index]["title"];
                            }

                          });

                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Property Name".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),
                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.transactionTypeController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        cursorWidth: 0,
                        onTap: (){
                          AppDialog.dialogSingleSelect(
                              context,
                              currentIndex: addTransactionController.indexTransactionType,
                              listSelect: addTransactionController.listTransactionType,
                              title: "Please select transaction type".tr,
                              selectedIndex: (int index){
                                addTransactionController.indexTransactionType = index;

                                if(addTransactionController.indexTransactionType!=-1){
                                  addTransactionController.transactionTypeController.text = addTransactionController.listTransactionType[index]["title"];
                                }

                              });
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Transaction type".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),


                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.tenantNameController,
                        keyboardType: TextInputType.none,
                        onTap: (){
                          AppDialog.dialogSingleSelect(
                              context,
                              currentIndex: addTransactionController.indexTenants,
                              listSelect: addTransactionController.listTenants,
                              title: "Please select tenants name".tr,
                              selectedIndex: (int index){
                                addTransactionController.indexTenants = index;
                                if(addTransactionController.indexTenants!=-1){
                                  addTransactionController.tenantNameController.text = addTransactionController.listTenants[index]["title"];
                                }

                              });
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Tenants name".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.dateController,
                        keyboardType: TextInputType.none,
                        onTap: ()async{
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100));

                          if (pickedDate != null) {
                            addTransactionController.dateController.text = DateFormat.yMMMd().format(pickedDate);
                          }


                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined),
                            labelText: "Payment Date".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.dueDateController,
                        keyboardType: TextInputType.none,
                        cursorWidth: 0,
                        cursorHeight: 0,
                        onTap: ()async{
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100));

                          if (pickedDate != null) {
                            addTransactionController.dueDateController.text = DateFormat.yMMMd().format(pickedDate);
                          }
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined),
                            labelText: "Due Date".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.paymentTypeController,
                        keyboardType: TextInputType.none,
                        cursorWidth: 0,
                        cursorHeight: 0,
                        onTap: (){
                          AppDialog.dialogSingleSelect(
                              context,
                              currentIndex: addTransactionController.indexPaymentType,
                              listSelect: addTransactionController.listPaymentType,
                              title: "Please select payment type".tr,
                              selectedIndex: (int index){
                                addTransactionController.indexPaymentType = index;

                                if(addTransactionController.indexPaymentType!=-1){
                                  addTransactionController.paymentTypeController.text = addTransactionController.listPaymentType[index]["title"];
                                }

                              });
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Payment type".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),


                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: addTransactionController.noteController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Note".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),
                    ],
                  ),
                ),

              )
            ],
          ),
        ),
      ),

      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
        child: ElevatedButton(
          onPressed: () {
            if(keyForm.currentState!.validate()){
              Get.back();
            }

          },
          child: Container(
            padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
            child: Text(
              "Save".tr,
              style: AppFont.textStyleTitle(
                color: AppColor.whiteColor,
                fontSize:
                AppDimension.getSizeSubTextTitleAddPropertyPage(context),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
