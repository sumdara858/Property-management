import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_string/string_format.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/controller/auth_controller/login_controller.dart';
import 'package:propertymanagement/page/auth/sign_up_page.dart';

import '../../app_style/app_font.dart';

class LoginPage extends StatefulWidget {
  
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {


  LoginController loginController = Get.put(LoginController());

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.primaryColor,
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                const SizedBox(height: AppDimension.appSpaceVertical*5,),

                Center(child: Image.asset("assets/images/logo_start_app.png",width: AppDimension.getSizeContainerPicture(context)*1.5,height: AppDimension.getSizeContainerPicture(context)*1.5,)),

                const SizedBox(height: AppDimension.appSpaceVertical*3,),

                TextFormField(
                  controller: loginController.emailController,
                  validator: StringFormat.validateEmail,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.email_outlined,color: AppColor.whiteColor),
                      labelText: "Email".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:  const OutlineInputBorder(),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical*2,),

                TextFormField(
                  controller: loginController.passwordController,
                  obscureText: loginController.isHidePassword.value,
                  validator: StringFormat.validatePassword,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor),
                  decoration: InputDecoration(
                      suffixIcon: InkWell(
                        onTap: (){
                          setState(() {
                            loginController.isHidePassword.value = !loginController.isHidePassword.value;
                          });
                         },
                        child: Obx((){

                          if(loginController.isHidePassword.value){
                            return  Icon(FontAwesomeIcons.eyeSlash,color: AppColor.whiteColor);
                          }

                          return  Icon(Icons.remove_red_eye_outlined,color: AppColor.whiteColor);
                        }),
                      ),
                      prefixIcon:   Icon(Icons.password_outlined,color: AppColor.whiteColor,),
                      labelText: "Password".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                        borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical*1.5,),

                Align(
                  alignment: Alignment.topRight,
                    child: InkWell(
                        onTap: (){

                        },
                        child: Text("Forget Password".tr,textAlign: TextAlign.end,style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400,fontSize: AppDimension.getTextSizeSubtitleItemNotification(context)),)
                    )
                ),

                const SizedBox(height: AppDimension.appSpaceVertical*1.5,),

                Container(
                  padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStatePropertyAll(AppColor.whiteColor)
                    ),
                      onPressed: (){
                        if(formKey.currentState!.validate()){
                          loginController.loginUser(context);
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.center,
                        child: Text("Login".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context),color: AppColor.primaryColor),),
                      )),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Don't have any account?".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w400,color: AppColor.whiteColor),),
                    const SizedBox(width: AppDimension.appSpaceVertical/2,),
                    InkWell
                      (
                        onTap: (){
                          Get.to(()=>SignUpPage());
                        },
                        child: Text("SIGN UP".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w600,color: AppColor.whiteColor),)
                      ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
