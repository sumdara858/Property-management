import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/controller/auth_controller/sign_up_controller.dart';
import 'package:propertymanagement/page/auth/login_page.dart';

import '../../app_string/string_format.dart';
import '../../app_style/app_color.dart';
import '../../app_style/app_font.dart';

class SignUpPage extends StatefulWidget {


  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  SignUpController signUpController = Get.put(SignUpController());

  final keyForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child:  Form(
            key: keyForm,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: AppDimension.appSpaceVertical*8,),
                Text(
                  "Create Account".tr,
                  style: AppFont.textStyleHeader(
                      color: AppColor.whiteColor,
                      fontSize: AppDimension.getSizeTextAppBar(context))
                ),
                Text(
                    "Create an account to continue!".tr,
                    style: AppFont.textStyleHeader(
                        color: AppColor.whiteColor,
                        fontWeight: FontWeight.w400,
                        fontSize: AppDimension.getTextSizeHeaderItemNotification(context))
                ),

                const SizedBox(height: AppDimension.appSpaceVertical*2.5,),



                TextFormField(
                  controller: signUpController.nameController,
                  validator: StringFormat.validateForm,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.person_outline,color: AppColor.whiteColor),
                      labelText: "Name".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                        borderSide: BorderSide(color: AppColor.greyColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                TextFormField(
                  controller: signUpController.emailController,
                  validator: StringFormat.validateEmail,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.email_outlined,color: AppColor.whiteColor),
                      labelText: "Email".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.greyColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                TextFormField(
                  controller: signUpController.phoneNumberController,
                  validator: StringFormat.validatePhoneNumber,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.phone_outlined,color: AppColor.whiteColor),
                      labelText: "Phone number".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.greyColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                TextFormField(
                  controller: signUpController.passwordController,
                  validator: StringFormat.validatePassword,
                  obscureText: signUpController.isHidePassword.value,
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.password_outlined,color: AppColor.whiteColor),
                      suffixIcon: InkWell(
                        onTap: (){
                          setState(() {
                            signUpController.isHidePassword.value = !signUpController.isHidePassword.value;
                          });
                        },
                        child: Obx((){

                          if(signUpController.isHidePassword.value){
                            return  Icon(FontAwesomeIcons.eyeSlash,color: AppColor.whiteColor);
                          }

                          return  Icon(Icons.remove_red_eye_outlined,color: AppColor.whiteColor);
                        }),
                      ),
                      labelText: "Password".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.greyColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                TextFormField(
                  controller: signUpController.confirmPasswordController,
                  obscureText: signUpController.isHideConfirmPassword.value,
                  validator: (value){
                    return StringFormat.validateConfirmPassword(signUpController.passwordController.text, value);
                  },
                  style: AppFont.textStyleTitle(color: AppColor.whiteColor,fontWeight: FontWeight.w400),
                  decoration: InputDecoration(
                      prefixIcon:  Icon(Icons.password_outlined,color: AppColor.whiteColor),
                      suffixIcon: InkWell(
                        onTap: (){
                          setState(() {
                            signUpController.isHideConfirmPassword.value = !signUpController.isHideConfirmPassword.value;
                          });
                        },
                        child: Obx((){

                          if(signUpController.isHideConfirmPassword.value){
                            return  Icon(FontAwesomeIcons.eyeSlash,color: AppColor.whiteColor);
                          }

                          return  Icon(Icons.remove_red_eye_outlined,color: AppColor.whiteColor);
                        }),
                      ),
                      labelText: "Confirm Password".tr,
                      fillColor: AppColor.primaryColor.withOpacity(0.5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.whiteColor)
                      ),
                      labelStyle: AppFont.hintTextFiledStyle(color: AppColor.whiteColor.withOpacity(0.7)),
                      border:   OutlineInputBorder(
                          borderSide: BorderSide(color: AppColor.greyColor)
                      ),
                      contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                  ),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical*1.5,),

                Container(
                  padding: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
                  child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(AppColor.whiteColor)
                      ),
                      onPressed: (){
                        if(keyForm.currentState!.validate()){
                          signUpController.signUpUser(context);
                        }
                      },
                      child: Container(
                        padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.center,
                        child: Text("Sign UP".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context),color: AppColor.primaryColor),),
                      )),
                ),

                const SizedBox(height: AppDimension.appSpaceVertical,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Already have account?".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w400,color: AppColor.whiteColor),),
                    const SizedBox(width: AppDimension.appSpaceVertical/2,),
                    InkWell
                      (
                        onTap: (){
                          Get.off(()=>LoginPage());
                        },
                        child: Text("SIGN IN".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextTitleItemProperty(context),fontWeight: FontWeight.w600,color: AppColor.whiteColor),)
                    ),
                  ],
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
