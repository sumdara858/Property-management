import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../app_style/app_color.dart';
import '../app_style/app_dimension.dart';
import '../app_style/app_font.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PdfFilePage extends StatefulWidget {

  final String urlPdf;

  PdfFilePage({
    required this.urlPdf
  });

  @override
  State<PdfFilePage> createState() => _PdfFilePageState();
}

class _PdfFilePageState extends State<PdfFilePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "File Details".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),
      body: SfPdfViewer.network(
       widget.urlPdf
      ),
    );
  }
}
