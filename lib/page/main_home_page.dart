import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:connectivity_plus_platform_interface/src/enums.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';
import 'package:propertymanagement/app_local_database/app_local_database.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';
import 'package:propertymanagement/page/add_document_page/document_list_page.dart';
import 'package:propertymanagement/page/add_property_page/property_list_page.dart';
import 'package:propertymanagement/page/auth/login_page.dart';
import 'package:propertymanagement/page/dash_board_page/dashboard_page.dart';
import 'package:propertymanagement/page/profile/profile_page.dart';
import 'package:propertymanagement/page/report/add_report_page.dart';
import 'package:propertymanagement/page/tenants_page/tenants_page.dart';
import 'package:propertymanagement/page/transaction_page/transaction_list_page.dart';
import 'package:propertymanagement/widget/menu_drawer.dart';
import 'package:visibility_aware_state/visibility_aware_state.dart';

import '../utils/dialog_no_internet_connection.dart';
import 'add_property_page/property_page.dart';
import 'cash_management_page/cash_management_page.dart';
import 'notification_page/notification_page.dart';

class MainHomePage extends StatefulWidget {
  int indexBottomNavigation = 2;

  MainHomePage({required this.indexBottomNavigation});

  @override
  State<MainHomePage> createState() => _MainHomePageState();
}

class _MainHomePageState extends VisibilityAwareState<MainHomePage> {

  String title = "";

  late StreamSubscription<ConnectivityResult> subscriptionNetwork;



  @override
  void onVisibilityChanged(WidgetVisibility visibility) {
    super.onVisibilityChanged(visibility);

    if (visibility == WidgetVisibility.VISIBLE) {
      subscriptionNetwork = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) async{


        if (result == ConnectivityResult.wifi ||
            result == ConnectivityResult.mobile) {

        }

        if (result == ConnectivityResult.none) {
          dialogNoInternetConnection(Get.context!);
        }
      });
    }

    if (visibility == WidgetVisibility.GONE ||
        visibility == WidgetVisibility.INVISIBLE) {
      subscriptionNetwork.cancel();
    }
  }


  final GlobalKey<ScaffoldState> keyScaffold = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: keyScaffold,
        appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          leading: IconButton(
            splashRadius: AppDimension.smallPadding,
            iconSize:AppDimension.getSizeIconBottomNavigation(context),
            onPressed: () {
              keyScaffold.currentState?.openDrawer();
            },
            icon: Image.asset('assets/icons/menu.png',color: AppColor.whiteColor,width: AppDimension.normalSizeIcon+8,height: AppDimension.normalSizeIcon+8,),
          ),
          actions: [
            IconButton(
              onPressed: (){
                setState(() {
                  widget.indexBottomNavigation  = 3;
                });
              },
              icon: Badge(
                  backgroundColor: AppColor.orangeColor,
                  label: Text("9+",style:AppFont.textStyleNormal(fontWeight: FontWeight.bold,fontSize: 8,color: AppColor.whiteColor),),
                  child: Icon(Icons.notifications_outlined,color: AppColor.whiteColor,size: AppDimension.getSizeIconBottomNavigation(context),)
              ),
            ),
          ],
          title: Builder(
            builder: (context) {
              switch (widget.indexBottomNavigation) {
                case 0:
                  {
                    title = "Property".tr;
                    break;
                  }

                case 1:
                  {
                    title = "Profile Details".tr;
                    break;
                  }

                case 3:
                  {
                    title = "Notification".tr;
                    break;
                  }

                case 4:
                  {
                    title = "Tenants".tr;
                    break;
                  }

                default:
                  {
                    title = "Dashboard".tr;
                  }
              }
              return Text(
                title,
                style: AppFont.textStyleHeader(color: AppColor.whiteColor,fontSize: AppDimension.getSizeTextAppBar(context)),
              );
            },
          ),
        ),
        drawer: Drawer(
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          elevation: AppDimension.defaultElevation,
          shape: const RoundedRectangleBorder(
          ),
          child: Column(
            children: [
              
              //*header
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: AppColor.primaryColor
                ),
                child: Column(
                  children: [
                    //*user profile
                    const SizedBox(
                      height: AppDimension.profileUserSize,
                      width: AppDimension.profileUserSize,
                      child: CircleAvatar(
                      backgroundImage: NetworkImage('https://picsum.photos/seed/picsum/200/300'),
                      ),
                    ),

                    const SizedBox(height: AppDimension.appSpaceVertical,),

                    Text("UserName",style: AppFont.textStyleHeader(color: AppColor.whiteColor,fontWeight: FontWeight.w600,fontSize: 18),),

                    const SizedBox(height: AppDimension.appSpaceVertical,)
                  ],
                ),
              ),
              
              SizedBox(
                height: MediaQuery.of(context).size.height-175,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //first section
                      Container(
                        decoration: BoxDecoration(
                            color: AppColor.whiteColor,
                            boxShadow: kElevationToShadow[1]
                        ),
                        margin:const EdgeInsets.all(AppDimension.appSpaceVertical),
                        //padding: const EdgeInsets.all(AppDimension.smallPadding),
                        child: Column(
                          children: [
                            MenuDrawer(title: "Dashboard".tr, iconsName: Icons.dashboard_outlined, onTap: (){
                              Get.back();
                              setState(() {
                                widget.indexBottomNavigation = 2;
                              });
                            }),
                            MenuDrawer(title: "Property list".tr, iconsName: Icons.business_outlined, onTap: (){
                              Get.back();
                              Get.to(()=>PropertyListPage());
                            }),
                            MenuDrawer(title: "Profile".tr, iconsName: Icons.person_outline, onTap: (){
                              Get.back();
                              setState(() {
                                widget.indexBottomNavigation = 1;
                              });
                            }),
                            MenuDrawer(title: "Notification".tr, iconsName: Icons.notifications_outlined, onTap: (){
                              Get.back();
                              setState(() {
                                widget.indexBottomNavigation = 3;
                              });
                            }),
                            MenuDrawer(title: "Tenants".tr, iconsName: Icons.group_outlined, onTap: (){
                              Get.back();
                              setState(() {
                                widget.indexBottomNavigation = 4;
                              });
                            })
                          ],
                        ),
                      ),

                      //second section
                      Container(
                        decoration: BoxDecoration(
                            color: AppColor.whiteColor,
                            boxShadow: kElevationToShadow[1]
                        ),
                        margin:const EdgeInsets.only(left: AppDimension.appSpaceVertical,right: AppDimension.appSpaceVertical),
                        //padding: const EdgeInsets.all(AppDimension.smallPadding),
                        child: Column(
                          children: [
                            MenuDrawer(title: "Transaction".tr, iconsName: FontAwesomeIcons.moneyBillTransfer, onTap: (){
                              Get.back();
                              Get.to(()=>TransactionListPage());
                            }),
                            MenuDrawer(title: "Document".tr, iconsName: FontAwesomeIcons.file, onTap: (){
                              Get.back();
                              Get.to(()=>DocumentPage());
                            }),
                            MenuDrawer(title: "Report".tr, iconsName:FontAwesomeIcons.message, onTap: (){
                              Get.back();
                              Get.to(()=>AddReportPage());
                            }),
                            MenuDrawer(title: "Cash Management".tr, iconsName: FontAwesomeIcons.fileInvoice, onTap: (){
                              Get.back();
                              Get.to(()=>CashManagementPage());
                            }),
                          ],
                        ),
                      ),

                      //Third section
                      Container(
                        decoration: BoxDecoration(
                            color: AppColor.whiteColor,
                            boxShadow: kElevationToShadow[1]
                        ),
                        margin:const EdgeInsets.only(left: AppDimension.appSpaceVertical,right: AppDimension.appSpaceVertical,top: AppDimension.appSpaceVertical),
                        //padding: const EdgeInsets.all(AppDimension.smallPadding),
                        child: Column(
                          children: [
                            MenuDrawer(title: "Setting".tr, iconsName: Icons.settings_outlined, onTap: (){
                              Get.back();
                            }),
                            MenuDrawer(title: "Logout".tr, iconsName: Icons.logout_outlined, onTap: ()async{
                              Get.back();
                              await AppLocalDataBase.setUserLogin(false);
                              Get.offAll(()=>LoginPage());
                            }),
                          ],
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical*3,)
                    ],
                  ),
                ),
              )
            ],
          ),

        ),
        body: Builder(builder: (BuildContext context){

          if(widget.indexBottomNavigation==1){
            return ProfilePage();
          }

          if(widget.indexBottomNavigation==3){
            return NotificationPage();
          }

          if(widget.indexBottomNavigation==0){
            return PropertyPage();
          }

          if(widget.indexBottomNavigation==4){
            return TenantsPage();
          }

          return DashBoardPage(
            onClickButtonTenants: (int index){
              setState(() {
                widget.indexBottomNavigation=4;
              });
            },
          );
        }
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          width: AppDimension.getSizeIconMiddleBottomNavigation(context),
          height: AppDimension.getSizeIconMiddleBottomNavigation(context),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: AppColor.whiteColor,
            boxShadow: kElevationToShadow[2],
          ),
          padding: const EdgeInsets.all(AppDimension.smallPadding),
          child: InkWell(
            onTap: () {
              setState(() {
                widget.indexBottomNavigation = 2;
              });
            },
            child: CircleAvatar(
              backgroundColor: widget.indexBottomNavigation == 2
                  ? AppColor.primaryColor
                  : AppColor.primaryColor.withOpacity(0.5),
              child: Icon(
                Icons.qr_code,
                size: AppDimension.getIconSizeFloatingButton(context),
                color: AppColor.whiteColor,
              ),
            ),
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: widget.indexBottomNavigation,
          fixedColor: AppColor.primaryColor,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              widget.indexBottomNavigation = index;
            });
          },
          items:  [
             BottomNavigationBarItem(
                icon: Icon(Icons.business_outlined,size: AppDimension.getSizeIconBottomNavigation(context)), label: "Property"),
             BottomNavigationBarItem(
                icon: Icon(Icons.person_outline,size: AppDimension.getSizeIconBottomNavigation(context),), label: "Profile"),
            BottomNavigationBarItem(
                icon: IgnorePointer(
                  ignoring: false,
                  child: Visibility(
                      visible: false,
                      child: Icon(Icons.qr_code,size: AppDimension.getSizeIconBottomNavigation(context))
                  ),
                ),
                label: ""),
            BottomNavigationBarItem(
                icon: Badge(
                    backgroundColor: AppColor.orangeColor,
                    label: Text("9+",style: AppFont.textStyleNormal(fontWeight: FontWeight.bold,fontSize: 8,color: AppColor.whiteColor),),
                    child:  Icon(Icons.notifications_outlined,size: AppDimension.getSizeIconBottomNavigation(context))
                ),
                label: "Notification"),
             BottomNavigationBarItem(icon: Icon(Icons.group_outlined,size: AppDimension.getSizeIconBottomNavigation(context)), label: "Tenants")
          ],
        ));
  }







}
