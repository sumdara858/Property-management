import 'package:country_list_pick/country_list_pick.dart';
import 'package:country_list_pick/country_selection_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:propertymanagement/controller/proprty_controller/add_property_controller.dart';

import 'dart:io';
import '../../app_dialog_select/app_dialog_select.dart';
import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../../helper/select_file_helper.dart';
import '../../utils/dialog_select_image.dart';

class AddPropertyPage extends StatefulWidget {
  @override
  State<AddPropertyPage> createState() => _AddPropertyPageState();
}

class _AddPropertyPageState extends State<AddPropertyPage> {


  final keyForm = GlobalKey<FormState>();

  AddPropertyController addPropertyController = Get.put(AddPropertyController());

  File? fileImage;

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
      elevation: AppDimension.defaultElevation,
      backgroundColor: Theme.of(context).primaryColor,
      leading: InkWell(
        onTap: (){
          Get.back();
        },
        child: const Icon(Icons.arrow_back),
      ),
      title: Text(
        "Add Property".tr,
        style: AppFont.textStyleHeader(color: AppColor.whiteColor,fontSize: AppDimension.getSizeTextAppBar(context)),
      )
     ),
      
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          decoration: BoxDecoration(
            boxShadow: kElevationToShadow[2],
            color: AppColor.whiteColor,
            borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(height: AppDimension.appSpaceVertical,),
              Text("General information".tr,style: AppFont.textStyleHeader(fontWeight: FontWeight.w600,fontSize: AppDimension.getSizeTextAppBar(context)-2),textAlign: TextAlign.center,),
              const SizedBox(height: AppDimension.appSpaceVertical,),
              Form(
                 key: keyForm,
                  child: Column(
                    children: [

                    TextFormField(
                      controller: addPropertyController.propertyNameController,
                      keyboardType: TextInputType.text,
                      validator: (value){
                        if(value!.isEmpty){
                          return "*Required";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: "Property name".tr,
                          labelStyle: AppFont.hintTextFiledStyle(),
                          border:  const OutlineInputBorder(),
                          contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                      ),
                    ),

                    const SizedBox(height: AppDimension.appSpaceVertical,) ,

                      TextFormField(
                        controller: addPropertyController.propertyTypeController,
                        keyboardType: TextInputType.none,
                        cursorWidth: 0,
                        cursorHeight: 0,
                        onTap: (){
                          AppDialog.dialogSingleSelect(context,title: "Please select Property type".tr,currentIndex: addPropertyController.indexPropertyType,listSelect: addPropertyController.listPropertyType,selectedIndex: (int index){
                            addPropertyController.indexPropertyType = index;

                            if(addPropertyController.indexPropertyType!=-1){
                              addPropertyController.propertyTypeController.text = addPropertyController.listPropertyType[index]["title"];
                            }
                          });
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.arrow_drop_down),
                            labelText: "Property type".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,

                      TextFormField(
                        controller: addPropertyController.categoryController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        cursorWidth: 0,
                        onTap: (){
                          AppDialog.dialogSingleSelect(context,title: "Please Select Category".tr,listSelect: addPropertyController.listCategoryType,currentIndex: addPropertyController.indexListCategory,selectedIndex: (int index){
                            addPropertyController.indexListCategory = index;
                            if(addPropertyController.indexListCategory!=-1){
                              addPropertyController.categoryController.text = addPropertyController.listCategoryType[index]["title"];
                            }

                          });
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.arrow_drop_down),
                            labelText: "Category".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,

                      TextFormField(
                        controller: addPropertyController.addressController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          suffixIcon: const Icon(Icons.location_on_outlined),
                            labelText: "Address".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,

                      TextFormField(
                        controller: addPropertyController.countryController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          prefixIcon: Center(
                            child: CountryListPick(
                              onChanged: (country){
                                addPropertyController.countryController.text = country!.name.toString();
                              },
                                appBar: AppBar(
                                  backgroundColor: AppColor.primaryColor,
                                  title:  Text('Select Country'.tr),
                                ),
                                theme: CountryTheme(
                                  isShowFlag: true,
                                  isShowTitle: true,
                                  isShowCode: false,
                                  isDownIcon: false,
                                  showEnglishName: true,
                                ),
                                // Set default value
                                initialSelection: '+855',
                                useUiOverlay: true,
                                useSafeArea: false
                            ),
                          ),
                            labelText: "Country".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,
                      TextFormField(
                        controller: addPropertyController.propertyNameController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "City".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,
                      TextFormField(
                        controller: addPropertyController.propertyNameController,
                        keyboardType: TextInputType.text,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Postal code".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,) ,
                      Align(
                        alignment: Alignment.topLeft,
                        child: GestureDetector(
                          onTap: () {
                            //select image
                            if (fileImage == null) {
                              dialogSelectImage(
                                  context,
                                  onSelectCamera: () async {
                                    SelectFileHelper.getImage(imageSource: ImageSource.camera).then((value){
                                      setState(() {
                                        fileImage = value;
                                        Get.back();
                                      });
                                    });
                                  },
                                  onSelectGallery: () async {
                                    SelectFileHelper.getImage(imageSource:ImageSource.gallery).then((value){
                                      setState(() {
                                        fileImage = value;
                                        Get.back();
                                      });
                                    });
                                  }
                              );
                            }
                          },
                          child: Container(
                            width: AppDimension.getSizeContainerPicture(context),
                            height: AppDimension.getSizeContainerPicture(context),
                            decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(AppDimension.defaultRadius),
                                border:
                                Border.all(color: AppColor.primaryColor, width: 1.5)),
                            child: Builder(
                              builder: (context) {
                                if (fileImage == null) {
                                  return Icon(
                                    Icons.add,
                                    color: AppColor.primaryColor,
                                    size: AppDimension.getSizeIcon(context),
                                  );
                                }
                                return Image.file(
                                  fileImage!,
                                  fit: BoxFit.cover,
                                );
                              },
                            ),
                          ),
                        ),
                      ),


                    ],)
              )

            ],
          ),
        ),
      ),

      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
        child: ElevatedButton(
          onPressed: () {
            if(keyForm.currentState!.validate()){
              Get.back();
            }

          },
          child: Container(
            padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
            child: Text(
              "Save".tr,
              style: AppFont.textStyleTitle(
                color: AppColor.whiteColor,
                fontSize:
                AppDimension.getSizeSubTextTitleAddPropertyPage(context),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
