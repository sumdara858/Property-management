import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/page/add_property_page/add_property_page.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../../widget/item_dashboard/item_property.dart';

class PropertyListPage extends StatefulWidget {


  @override
  State<PropertyListPage> createState() => _PropertyListPageState();
}

class _PropertyListPageState extends State<PropertyListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Properties".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),
      floatingActionButton: CircleAvatar(
        radius:MediaQuery.of(context).size.width>720?AppDimension.getSizeIcon(context)/1.5:AppDimension.getSizeIcon(context),
        backgroundColor: Theme.of(context).primaryColor,
        child: IconButton(
            onPressed: ()async{
              Get.to(()=>AddPropertyPage());
            },
            icon: Icon(Icons.add,color: AppColor.whiteColor,size: AppDimension.getSizeIcon(context))
        ),
      ).animate().slideX(begin: -2.5,end: 0),
      body: Container(
        margin:  const EdgeInsets.all(AppDimension.appSpaceVertical),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical*1.5),
                    decoration: BoxDecoration(
                        color: AppColor.primaryColor,
                        borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                    ),
                    child: Column(
                      children: [
                        Text("Total".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500)),
                        const SizedBox(height: AppDimension.appSpaceVertical/3,),
                        Text("10".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: AppDimension.appSpaceVertical/2,),
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical*1.5),
                    decoration: BoxDecoration(
                        color: AppColor.blueColor,
                        borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                    ),
                    child: Column(
                      children: [
                        Text("Occupied".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500),),
                        const SizedBox(height: AppDimension.appSpaceVertical/3,),
                        Text("7".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
                const SizedBox(width: AppDimension.appSpaceVertical/2,),

                Expanded(
                  child: Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical*1.5),
                    decoration: BoxDecoration(
                        color: AppColor.orangeColor,
                        borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
                    ),
                    child: Column(
                      children: [
                        Text("Vacant".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context),color: AppColor.whiteColor,fontWeight: FontWeight.w500),),
                        const SizedBox(height: AppDimension.appSpaceVertical/3,),
                        Text("3".tr,style: AppFont.textStyleTitle(fontSize: AppDimension.getSizeTextInformationProfilePage(context)+4,color: AppColor.whiteColor,fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(height: AppDimension.appSpaceVertical,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width:MediaQuery.of(context).size.width>720?MediaQuery.of(context).size.width*0.88:MediaQuery.of(context).size.width*0.8,
                    child: TextField(
                      decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.search),
                          filled: true,
                          fillColor: AppColor.whiteColor,
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                              borderSide: BorderSide(
                                  color: AppColor.greyColor,
                                  width: 0.5
                              )
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius:  BorderRadius.circular(AppDimension.defaultRadius),
                              borderSide: BorderSide(
                                  color: AppColor.greyColor,
                                  width: 0.5
                              )
                          ),
                          contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical)
                      ),

                    )
                ),
                const Spacer(),
                Container(
                  padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                  decoration: BoxDecoration(
                      borderRadius:BorderRadius.circular(AppDimension.defaultRadius),
                      color: AppColor.whiteColor
                  ),
                  child: InkWell(
                    onTap: (){
                    },
                    child: Icon(FontAwesomeIcons.barsProgress,color: AppColor.primaryColor,size:MediaQuery.of(context).size.width>720? AppDimension.getSizeIcon(context)-16:AppDimension.getSizeIcon(context),),
                  ),
                ),

                const SizedBox(width: AppDimension.appSpaceVertical/2,),
              ],
            ),

            const SizedBox(height: AppDimension.appSpaceVertical,),

            Text("Property List".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context),fontWeight: FontWeight.w600),),


            const SizedBox(height: AppDimension.appSpaceVertical,),

            SizedBox(
              height: MediaQuery.of(context).size.height*0.65-25,
              child: SingleChildScrollView(
                child: Column(
                   children:  List.generate(10, (index) {
                     if (index == 9) {
                       return Container(
                           margin: const EdgeInsets.only(
                               bottom: AppDimension.appSpaceVertical),
                           child: ItemProperty(
                             onTap: () {},
                             onTapMenu: () {},
                           ));
                     }
                     return ItemProperty(
                       onTap: () {},
                       onTapMenu: () {},
                     );
                   }),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
