import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';
import 'package:propertymanagement/controller/tenants_controller/tenant_controller.dart';
import 'package:propertymanagement/helper/select_file_helper.dart';
import 'package:propertymanagement/utils/dialog_select_image.dart';

class AddTenantPage extends StatefulWidget {
  @override
  State<AddTenantPage> createState() => _AddTenantPageState();
}

class _AddTenantPageState extends State<AddTenantPage> {
  final keyForm = GlobalKey<FormState>();
  File? fileImage;
  TenantController tenantController  = Get.put(TenantController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Add Tenants".tr,
          style: AppFont.textStyleHeader(
              fontSize: AppDimension.getSizeTextAppBar(context),
              color: AppColor.whiteColor),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  //select image
                  if (fileImage == null) {
                    dialogSelectImage(
                        context,
                        onSelectCamera: () async {
                          SelectFileHelper.getImage(imageSource: ImageSource.camera).then((value){
                            setState(() {
                              fileImage = value as File?;
                              Get.back();
                            });
                          });
                         },
                        onSelectGallery: () async {
                          SelectFileHelper.getImage(imageSource:ImageSource.gallery).then((value){
                            setState(() {
                              fileImage = value as File?;
                              Get.back();
                            });
                          });
                        }
                        );
                  }
                },
                child: Container(
                  width: AppDimension.getSizeContainerPicture(context),
                  height: AppDimension.getSizeContainerPicture(context),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AppDimension.defaultRadius),
                      border:
                          Border.all(color: AppColor.primaryColor, width: 1.5)),
                  child: Builder(
                    builder: (context) {
                      if (fileImage == null) {
                        return Icon(
                          Icons.add,
                          color: AppColor.primaryColor,
                          size: AppDimension.getSizeIcon(context),
                        );
                      }
                      return Image.file(
                        fileImage!,
                        fit: BoxFit.cover,
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(height: AppDimension.appSpaceVertical,),

              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                decoration: BoxDecoration(
                  color: AppColor.whiteColor,
                  borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                  boxShadow: kElevationToShadow[2]
                ),
                
                child: Form(
                  key: keyForm,
                  child: Column(
                    children: [
                      Text("Information Tenants".tr,style: AppFont.textStyleTitle(fontWeight: FontWeight.w600,fontSize: AppDimension.getTextSizeHeaderItemNotification(context)),),
                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.nameController,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          labelText: "Name".tr,
                          labelStyle: AppFont.hintTextFiledStyle(),
                          border:  const OutlineInputBorder(),
                          contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),
                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.phoneNumberController,
                        keyboardType: TextInputType.phone,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Phone number".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),


                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.emailController,
                        keyboardType: TextInputType.emailAddress,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Email".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.moveInDateController,
                        keyboardType: TextInputType.none,
                        onTap: ()async{
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100));

                          if (pickedDate != null) {
                            tenantController.moveInDateController.text = DateFormat.yMMMd().format(pickedDate);
                          }


                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined),
                            labelText: "Move in Date".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.moveOutDateController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        onTap: ()async{
                          DateTime? pickedDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2100));

                          if (pickedDate != null) {
                            tenantController.moveOutDateController.text = DateFormat.yMMMd().format(pickedDate);
                          }
                        },
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            suffixIcon: const Icon(Icons.calendar_month_outlined),
                            labelText: "Move out Date".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),

                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.advanceAmountController,
                        keyboardType: TextInputType.number,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Advance Amount".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),


                      const SizedBox(height: AppDimension.appSpaceVertical,),
                      TextFormField(
                        controller: tenantController.rentAmountController,
                        keyboardType: TextInputType.number,
                        validator: (value){
                          if(value!.isEmpty){
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Rent Amount".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border:  const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical/2)
                        ),
                      ),



                    ],
                  ),
                ),
                
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
        child: ElevatedButton(
          onPressed: () {
            if(keyForm.currentState!.validate()){
              Get.back();
            }

          },
          child: Container(
            padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
            child: Text(
              "Submit".tr,
              style: AppFont.textStyleTitle(
                color: AppColor.whiteColor,
                fontSize:
                    AppDimension.getSizeSubTextTitleAddPropertyPage(context),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
