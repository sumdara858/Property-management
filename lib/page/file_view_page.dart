import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../app_style/app_color.dart';
import '../app_style/app_dimension.dart';
import '../app_style/app_font.dart';

class FileViewPage extends StatefulWidget {

  final String fileExtension;
  final File file;

  FileViewPage(
  {
    required this.file,
    required this.fileExtension
  }
  );

  @override
  State<FileViewPage> createState() => _FileViewPageState();
}

class _FileViewPageState extends State<FileViewPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.fileExtension=="jpg"||widget.fileExtension=="png"?AppColor.blackColor:AppColor.whiteColor,
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Preview File".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),
      body: InteractiveViewer(
        child: Center(
          child: Builder(builder: (BuildContext context){
            
            
            if(widget.fileExtension=="pdf"){
              
              return SfPdfViewer.file(widget.file);
              
            }
            
            return Image.file(widget.file);
          },),
        ),
      ),
    );
  }
}
