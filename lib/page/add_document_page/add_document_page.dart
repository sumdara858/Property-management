import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/controller/add_document_controller/add_document_controller.dart';
import 'dart:io';
import '../../app_dialog_select/app_dialog_select.dart';
import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../file_view_page.dart';

class AddDocumentPage extends StatefulWidget {
  @override
  State<AddDocumentPage> createState() => _AddDocumentPageState();
}

class _AddDocumentPageState extends State<AddDocumentPage> {
  AddDocumentController addDocumentController =
      Get.put(AddDocumentController());

  final keyForm = GlobalKey<FormState>();

  File? file;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Add Document".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
              color: AppColor.whiteColor,
              boxShadow: kElevationToShadow[2]),
          child: Column(
            children: [
              Text(
                "Information Document".tr,
                style: AppFont.textStyleTitle(
                    fontWeight: FontWeight.w600,
                    fontSize: AppDimension.getTextSizeHeaderItemNotification(
                        context)),
              ),
              Form(
                  key: keyForm,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),
                      TextFormField(
                        controller: addDocumentController.tenantsNameController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        cursorWidth: 0,
                        onTap: () {
                          AppDialog.dialogSingleSelect(context,
                              currentIndex: addDocumentController.indexTenants,
                              listSelect: addDocumentController.listTenants,
                              title: "Please select property name".tr,
                              selectedIndex: (int index) {
                            addDocumentController.indexTenants = index;

                            if (addDocumentController.indexTenants != -1) {
                              addDocumentController.tenantsNameController.text =
                                  addDocumentController.listTenants[index]
                                      ["title"];
                            }
                          });
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Property Name".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border: const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical / 2)),
                      ),
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),
                      TextFormField(
                        controller: addDocumentController.fileNameController,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "File Name".tr,
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border: const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical / 2)),
                      ),
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),
                      GestureDetector(
                        onTap: () async {
                          //select file

                          if (file == null) {
                            FilePickerResult? result =
                                await FilePicker.platform.pickFiles(
                              allowMultiple: false,
                            );

                            if (result != null) {
                              final fileBytes =
                                  await File(result.files[0].path.toString())
                                      .readAsBytes();
                              setState(() {
                                file = File(result.files[0].path.toString());
                                addDocumentController.fileName =
                                    result.files[0].name.toString();
                                addDocumentController.fileExtension =
                                    result.files[0].extension.toString();
                                addDocumentController.fileSize =
                                    ((fileBytes.lengthInBytes / (1024 * 1024))
                                            .toStringAsFixed(3))
                                        .toString();
                              });
                            }
                          }
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.all(
                              AppDimension.appSpaceVertical * 1.5),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  AppDimension.defaultRadius),
                              border: Border.all(
                                  color:file==null?AppColor.primaryColor:AppColor.greyColor, width: 1.5)),
                          child: Builder(
                            builder: (context) {
                              if (file == null) {
                                return Text(
                                  "Choose a file".tr,
                                  style: AppFont.textStyleTitle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: AppDimension
                                          .getSizeTextTitleItemProperty(
                                              context)),
                                );
                              }
                              return InkWell(
                                onTap: (){
                                  Get.to(()=>FileViewPage(
                                    file: file!,
                                    fileExtension: addDocumentController.fileExtension,
                                  ));
                                },
                                child: Row(
                                  children: [
                                    Builder(builder: (BuildContext context) {
                                      if (addDocumentController.fileExtension ==
                                          "pdf") {
                                        return Icon(
                                          Icons.picture_as_pdf_outlined,
                                          color: AppColor.primaryColor,
                                          size:
                                              AppDimension.getSizeIcon(context) +
                                                  10,
                                        );
                                      }

                                      if (addDocumentController.fileExtension ==
                                          "doc") {
                                        return Icon(
                                          Icons.document_scanner_outlined,
                                          color: AppColor.primaryColor,
                                          size:
                                              AppDimension.getSizeIcon(context) +
                                                  10,
                                        );
                                      }

                                      return Image.file(file!,
                                          height:
                                              AppDimension.getSizeIcon(context) +
                                                  10,
                                          width:
                                              AppDimension.getSizeIcon(context) +
                                                  10);
                                    }),
                                    const SizedBox(
                                      width: AppDimension.appSpaceVertical,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            addDocumentController.fileName,
                                            style: AppFont.textStyleTitle(
                                              fontSize: AppDimension
                                                  .getSizeTextTitleItemProperty(
                                                      context),
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                          const SizedBox(
                                            height:
                                                AppDimension.appSpaceVertical / 2,
                                          ),
                                          Text(
                                            "${addDocumentController.fileSize} MB",
                                            style: AppFont.textStyleTitle(
                                              fontSize: AppDimension
                                                  .getSizeTextTitleItemProperty(
                                                      context),
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    InkWell(
                                      onTap: (){
                                        setState(() {
                                          file = null;
                                        });
                                      },
                                      child: Icon(
                                        FontAwesomeIcons.trashCan,
                                        color: AppColor.redColor,
                                        size: AppDimension.getSizeIcon(context)-6,
                                      ),
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(
                            top: AppDimension.appSpaceVertical,
                            bottom: AppDimension.appSpaceVertical),
                        child: ElevatedButton(
                          onPressed: () {
                            if (keyForm.currentState!.validate()) {
                              Get.back();
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical),
                            child: Text(
                              "Save".tr,
                              style: AppFont.textStyleTitle(
                                color: AppColor.whiteColor,
                                fontSize: AppDimension
                                    .getSizeSubTextTitleAddPropertyPage(
                                        context),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
