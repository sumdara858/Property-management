import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/page/add_document_page/add_document_page.dart';
import 'package:propertymanagement/page/pdf_file_page.dart';
import 'package:propertymanagement/widget/item_document/item_document_widget.dart';

import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';
import '../../widget/item_dashboard/item_transaction.dart';
import '../transaction_page/transaction_detail_page.dart';

class DocumentPage extends StatefulWidget {


  @override
  State<DocumentPage> createState() => _DocumentPageState();
}

class _DocumentPageState extends State<DocumentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Document".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),

      floatingActionButton: CircleAvatar(
        radius:MediaQuery.of(context).size.width>720?AppDimension.getSizeIcon(context)/1.5:AppDimension.getSizeIcon(context),
        backgroundColor: Theme.of(context).primaryColor,
        child: IconButton(
            onPressed: ()async{
              Get.to(()=>AddDocumentPage());
            },
            icon: Icon(Icons.add,color: AppColor.whiteColor,size: AppDimension.getSizeIcon(context))
        ),
      ).animate().slideX(begin: -2.5,end: 0),


      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                      width:MediaQuery.of(context).size.width>720?MediaQuery.of(context).size.width*0.88:MediaQuery.of(context).size.width*0.8,
                      child: TextField(
                        decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.search),
                            filled: true,
                            fillColor: AppColor.whiteColor,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
                                borderSide: BorderSide(
                                    color: AppColor.greyColor,
                                    width: 0.5
                                )
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:  BorderRadius.circular(AppDimension.defaultRadius),
                                borderSide: BorderSide(
                                    color: AppColor.greyColor,
                                    width: 0.5
                                )
                            ),
                            contentPadding: const EdgeInsets.all(AppDimension.appSpaceVertical)
                        ),

                      )
                  ),
                  const Spacer(),
                  Container(
                    padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
                    decoration: BoxDecoration(
                        borderRadius:BorderRadius.circular(AppDimension.defaultRadius),
                        color: AppColor.whiteColor
                    ),
                    child: InkWell(
                      onTap: (){
                      },
                      child: Icon(FontAwesomeIcons.barsProgress,color: AppColor.primaryColor,size:MediaQuery.of(context).size.width>720? AppDimension.getSizeIcon(context)-16:AppDimension.getSizeIcon(context),),
                    ),
                  ),

                  const SizedBox(width: AppDimension.appSpaceVertical/2,),
                ],
              ),
              const SizedBox(height: AppDimension.appSpaceVertical,),
              Text("Document List".tr,style: AppFont.textStyleHeader(fontSize: AppDimension.getSizeTextAppBar(context),fontWeight: FontWeight.w600),),
              const SizedBox(height: AppDimension.appSpaceVertical,),

              SizedBox(
                height: MediaQuery.of(context).size.height*0.75-5,
                child: SingleChildScrollView(
                  child: Column(
                    children: List.generate(10, (index) {
                      return ItemDocumentWidget(
                        onTap: (){
                          Get.to(()=>PdfFilePage(urlPdf: 'https://cdn.syncfusion.com/content/PDFViewer/flutter-succinctly.pdf'));
                        },
                        onTapMenu: (){},
                      );
                    }),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
