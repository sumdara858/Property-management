import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:propertymanagement/controller/report_controller/add_report_controller.dart';

import '../../app_dialog_select/app_dialog_select.dart';
import '../../app_style/app_color.dart';
import '../../app_style/app_dimension.dart';
import '../../app_style/app_font.dart';

class AddReportPage extends StatefulWidget {


  @override
  State<AddReportPage> createState() => _AddReportPageState();
}

class _AddReportPageState extends State<AddReportPage> {

  final keyForm = GlobalKey<FormState>();

  AddReportController addReportController = AddReportController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: AppDimension.defaultElevation,
          backgroundColor: Theme.of(context).primaryColor,
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: const Icon(Icons.arrow_back),
          ),
          title: Text(
            "Report".tr,
            style: AppFont.textStyleHeader(
                color: AppColor.whiteColor,
                fontSize: AppDimension.getSizeTextAppBar(context)),
          )),

      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
          margin: const EdgeInsets.all(AppDimension.appSpaceVertical),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
            color: AppColor.whiteColor,
            boxShadow: kElevationToShadow[2]
          ),
          child: Column(
            children: [
              Text(
                "Information Report".tr,
                style: AppFont.textStyleTitle(
                    fontWeight: FontWeight.w600,
                    fontSize: AppDimension.getTextSizeHeaderItemNotification(
                        context)),
              ),
              Form(
                  key: keyForm,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),
                      TextFormField(
                        controller: addReportController.propertyNameController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        cursorWidth: 0,
                        onTap: () {
                          AppDialog.dialogSingleSelect(context,
                              currentIndex: addReportController.indexPropertyName,
                              listSelect: addReportController.listProperty,
                              title: "Please select property name".tr,
                              selectedIndex: (int index) {
                                addReportController.indexPropertyName = index;

                                if (addReportController.indexPropertyName != -1) {
                                  addReportController.propertyNameController.text =
                                  addReportController.listProperty[index]["title"];
                                }
                              });
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Property Name".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border: const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical / 2)),
                      ),
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),
                      TextFormField(
                        controller: addReportController.tenantNameController,
                        keyboardType: TextInputType.none,
                        cursorWidth: 0,
                        cursorHeight: 0,
                        onTap: () {
                          AppDialog.dialogSingleSelect(context,
                              currentIndex: addReportController.indexTenants,
                              listSelect: addReportController.listTenants,
                              title: "Please select tenant name".tr,
                              selectedIndex: (int index) {
                                addReportController.indexTenants = index;

                                if (addReportController.indexTenants != -1) {
                                  addReportController.tenantNameController.text =
                                  addReportController.listTenants[index]["title"];
                                }
                              });
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Tenants Name".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border: const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical / 2)),
                      ),
                      const SizedBox(
                        height: AppDimension.appSpaceVertical,
                      ),

                      TextFormField(
                        controller: addReportController.monthController,
                        keyboardType: TextInputType.none,
                        cursorHeight: 0,
                        cursorWidth: 0,
                        onTap: () {
                          AppDialog.dialogSingleSelect(context,
                              currentIndex: addReportController.indexMonth,
                              listSelect: addReportController.listMonthly,
                              title: "Please select Month".tr,
                              selectedIndex: (int index) {
                                addReportController.indexMonth = index;
                                if (addReportController.indexMonth != -1) {
                                  addReportController.monthController.text =
                                  addReportController.listMonthly[index]["title"];
                                }
                              });
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "*Required";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Monthly".tr,
                            suffixIcon: const Icon(Icons.arrow_drop_down_sharp),
                            labelStyle: AppFont.hintTextFiledStyle(),
                            border: const OutlineInputBorder(),
                            contentPadding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical / 2)),
                      ),

                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(
                            top: AppDimension.appSpaceVertical,
                            bottom: AppDimension.appSpaceVertical),
                        child: ElevatedButton(
                          onPressed: () {
                            if (keyForm.currentState!.validate()) {
                              Get.back();
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.all(
                                AppDimension.appSpaceVertical),
                            child: Text(
                              "Save".tr,
                              style: AppFont.textStyleTitle(
                                color: AppColor.whiteColor,
                                fontSize: AppDimension
                                    .getSizeSubTextTitleAddPropertyPage(
                                    context),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ))
            ],
          )
        ),
      ),
    );
  }
}
