import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';
import 'package:propertymanagement/page/add_document_page/add_document_page.dart';
import 'package:propertymanagement/page/transaction_page/add_transaction_page.dart';
import 'package:propertymanagement/widget/item_dashboard/item_transaction.dart';
import 'package:propertymanagement/widget/menu_dashboard.dart';

import '../../widget/item_dashboard/item_property.dart';
import '../add_property_page/add_property_page.dart';
import '../add_property_page/property_list_page.dart';
import '../report/add_report_page.dart';
import '../tenants_page/add_tenant_page.dart';
import '../transaction_page/transaction_detail_page.dart';
import '../transaction_page/transaction_list_page.dart';

class DashBoardPage extends StatefulWidget {

  final ValueChanged<int>? onClickButtonTenants;

  DashBoardPage(
  {
    this.onClickButtonTenants
}
      );

  @override
  State<DashBoardPage> createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  double height = 110;

  double width = 110;

  ScrollController scrollController = ScrollController();

  bool isShowFloatingButton = true;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(showFloatingButton);
  }

  void showFloatingButton() {
    if (scrollController.position.extentBefore > 100) {
      setState(() {
        isShowFloatingButton = false;
      });
    } else {
      setState(() {
        isShowFloatingButton = true;
      });
    }
  }

  final keyRefresh = GlobalKey<RefreshIndicatorState>();

  Offset offsetFloatingButton = Offset(Get.width*0.85, Get.height*0.75);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: LayoutBuilder(
        builder: (context,constraints) {
          return Stack(
            children: [
              Positioned(
                  top: offsetFloatingButton.dy,
                  left: offsetFloatingButton.dx,
                  child: LongPressDraggable(
                    onDragEnd: (details){
                      print(details.offset.toString());
                      setState(() {
                        final adjustmentHeight = MediaQuery.of(context).size.height -
                            constraints.maxHeight;

                        final adjustmentWidth = MediaQuery.of(context).size.width -
                            constraints.maxWidth;

                        offsetFloatingButton = Offset(
                            details.offset.dx, details.offset.dy);

                        if(offsetFloatingButton.dy<0||offsetFloatingButton.dy==0){
                          offsetFloatingButton = Offset(
                              details.offset.dx, adjustmentHeight);
                        }

                        if(offsetFloatingButton.dx<0||offsetFloatingButton.dx==0){
                          offsetFloatingButton = Offset(
                              adjustmentWidth, details.offset.dy);
                        }


                      });
                    },
                    feedback: CircleAvatar(
                      radius: MediaQuery.of(context).size.width > 720
                          ? AppDimension.getSizeIcon(context) / 1.5
                          : AppDimension.getSizeIcon(context),
                      backgroundColor: Theme.of(context).primaryColor,
                      child: Builder(
                        builder: (context) {
                          if (isShowFloatingButton) {
                            return IconButton(
                                onPressed: () {
                                  scrollController.animateTo(
                                      scrollController.position.maxScrollExtent,
                                      duration: const Duration(seconds: 1),
                                      curve: Curves.linear);
                                },
                                icon: Icon(
                                  Icons.arrow_downward_rounded,
                                  color: AppColor.whiteColor,
                                  size: AppDimension.getSizeIcon(context),
                                )).animate().shake();
                          }

                          return IconButton(
                              onPressed: () {
                                scrollController.animateTo(0,
                                    duration: const Duration(seconds: 1),
                                    curve: Curves.linear);
                              },
                              icon: Icon(Icons.arrow_upward,
                                  color: AppColor.whiteColor,
                                  size: AppDimension.getSizeIcon(context)))
                              .animate()
                              .shake();
                        },
                      ),
                    ),
                    child: CircleAvatar(
                      radius: MediaQuery.of(context).size.width > 720
                          ? AppDimension.getSizeIcon(context) / 1.5
                          : AppDimension.getSizeIcon(context),
                      backgroundColor: Theme.of(context).primaryColor,
                      child: Builder(
                        builder: (context) {
                          if (isShowFloatingButton) {
                            return IconButton(
                                onPressed: () {
                                  scrollController.animateTo(
                                      scrollController.position.maxScrollExtent,
                                      duration: const Duration(seconds: 1),
                                      curve: Curves.linear);
                                },
                                icon: Icon(
                                  Icons.arrow_downward_rounded,
                                  color: AppColor.whiteColor,
                                  size: AppDimension.getSizeIcon(context),
                                )).animate().shake();
                          }

                          return IconButton(
                              onPressed: () {
                                scrollController.animateTo(0,
                                    duration: const Duration(seconds: 1),
                                    curve: Curves.linear);
                              },
                              icon: Icon(Icons.arrow_upward,
                                  color: AppColor.whiteColor,
                                  size: AppDimension.getSizeIcon(context)))
                              .animate()
                              .shake();
                        },
                      ),
                    ))
              )
            ],
          );
        }
      ),
      body: RefreshIndicator(
        key: keyRefresh,
        onRefresh: () async {},
        child: SingleChildScrollView(
          controller: scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: const EdgeInsets.symmetric(
                horizontal: AppDimension.appSpaceVertical,
                vertical: AppDimension.appSpaceVertical / 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: AppDimension.appSpaceVertical * 2,
                ),

                //*title
                Text(
                  "Welcome Back".tr,
                  style: AppFont.textStyleTitle(
                      fontWeight: FontWeight.w600,
                      color: AppColor.greyColor,
                      fontSize: AppDimension.getSizeTextAppBar(context)),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical,
                ),

                //*menu
                Builder(builder: (context) {
                  MenuDashBoard(
                    onTapCallBack: () {},
                    title: "Add Transaction",
                    imageName: "",
                    checkItemHeight: (double h) {
                      height = h;
                    },
                    checkItemWidth: (double w) {
                      width = w;
                    },
                  );
                  return SizedBox(
                    height:
                        AppDimension.getHeightMenuDashboard(height, context),
                    child: GridView(
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          childAspectRatio: height / width),
                      children: [
                        MenuDashBoard(
                            title: "Add Property".tr,
                            imageName: "assets/images/add_property.png",
                            onTapCallBack: () {
                              Get.to(()=>AddPropertyPage());
                            }),
                        MenuDashBoard(
                            title: "Tenants".tr,
                            imageName: "assets/images/tenants.png",
                            onTapCallBack: () {
                            widget.onClickButtonTenants!(4);
                            }),
                        MenuDashBoard(
                            title: "Add Transaction".tr,
                            imageName: "assets/images/add_transaction.png",
                            onTapCallBack: () {
                              Get.to(()=>AddTransactionPage());
                            }),
                        MenuDashBoard(
                            title: "Add Document".tr,
                            imageName: "assets/images/document.png",
                            onTapCallBack: () {
                              Get.to(()=>AddDocumentPage());
                            }),
                        MenuDashBoard(
                            title: "Report".tr,
                            imageName: "assets/images/report.png",
                            onTapCallBack: () {
                              Get.to(()=>AddReportPage());
                            }),
                        MenuDashBoard(
                            title: "Add Tenants".tr,
                            imageName: "assets/images/add_tenant.png",
                            onTapCallBack: () {
                              Get.to(()=>AddTenantPage());
                            })
                      ],
                    ),
                  );
                }),

                const SizedBox(
                  height: AppDimension.appSpaceVertical,
                ),

                Container(
                  padding: EdgeInsets.all(AppDimension.smallPadding *
                      AppDimension.getHeightButton(context)),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AppDimension.defaultRadius),
                      color: AppColor.blueColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total Property",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.w600,
                            color: AppColor.whiteColor),
                      ),
                      Text(
                        "3",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.bold,
                            color: AppColor.whiteColor),
                      )
                    ],
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical,
                ),

                Container(
                  padding: EdgeInsets.all(AppDimension.smallPadding *
                      AppDimension.getHeightButton(context)),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AppDimension.defaultRadius),
                      color: AppColor.primaryColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Occupied",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.w600,
                            color: AppColor.whiteColor),
                      ),
                      Text(
                        "10",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.bold,
                            color: AppColor.whiteColor),
                      )
                    ],
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical,
                ),

                Container(
                  padding: EdgeInsets.all(AppDimension.smallPadding *
                      AppDimension.getHeightButton(context)),
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(AppDimension.defaultRadius),
                      color: AppColor.orangeColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Vacant",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.w600,
                            color: AppColor.whiteColor),
                      ),
                      Text(
                        "18+",
                        style: AppFont.textStyleTitle(
                            fontSize:
                                AppDimension.getSizeSubTextTitleAddPropertyPage(
                                    context),
                            fontWeight: FontWeight.bold,
                            color: AppColor.whiteColor),
                      )
                    ],
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical * 2,
                ),

                Container(
                  padding: EdgeInsets.all(AppDimension.smallPadding *
                      AppDimension.getHeightButton(context)),
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(AppDimension.defaultRadius),
                          topRight:
                              Radius.circular(AppDimension.defaultRadius)),
                      color: AppColor.whiteColor,
                      boxShadow: kElevationToShadow[2]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Properties",
                        style: AppFont.textStyleTitle(
                            fontSize: AppDimension.getTextSizeSeeAll(context),
                            fontWeight: FontWeight.w600,
                            color: AppColor.blackColor),
                      ),
                      InkWell(
                        onTap: (){
                          Get.to(()=>PropertyListPage());
                        },
                        child: Text(
                          "See All".tr,
                          style: AppFont.textStyleTitle(
                              fontSize:
                                  AppDimension.getSizeSubTextTitleAddPropertyPage(
                                      context),
                              fontWeight: FontWeight.w500,
                              color: AppColor.blackColor),
                        ),
                      )
                    ],
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical / 2,
                ),

                Container(
                  decoration: BoxDecoration(
                      color: AppColor.whiteColor,
                      boxShadow: kElevationToShadow[2]),
                  child: Column(
                    children: List.generate(5, (index) {
                      if (index == 4) {
                        return Container(
                            margin: const EdgeInsets.only(
                                bottom: AppDimension.appSpaceVertical),
                            child: ItemProperty(
                              onTap: () {},
                              onTapMenu: () {},
                            ));
                      }
                      return ItemProperty(
                        onTap: () {},
                        onTapMenu: () {},
                      );
                    }),
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical * 2,
                ),

                Container(
                  padding: EdgeInsets.all(AppDimension.smallPadding *
                      AppDimension.getHeightButton(context)),
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(AppDimension.defaultRadius),
                          topRight:
                              Radius.circular(AppDimension.defaultRadius)),
                      color: AppColor.whiteColor,
                      boxShadow: kElevationToShadow[2]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Transaction History",
                        style: AppFont.textStyleTitle(
                            fontSize: AppDimension.getTextSizeSeeAll(context),
                            fontWeight: FontWeight.w600,
                            color: AppColor.blackColor),
                      ),
                      InkWell(
                        onTap: (){
                          Get.to(()=>TransactionListPage());
                        },
                        child: Text(
                          "See All".tr,
                          style: AppFont.textStyleTitle(
                              fontSize:
                                  AppDimension.getSizeSubTextTitleAddPropertyPage(
                                      context),
                              fontWeight: FontWeight.w500,
                              color: AppColor.blackColor),
                        ),
                      )
                    ],
                  ),
                ),

                const SizedBox(
                  height: AppDimension.appSpaceVertical / 2,
                ),

                Container(
                  decoration: BoxDecoration(
                      color: AppColor.whiteColor,
                      boxShadow: kElevationToShadow[2]),
                  padding: const EdgeInsets.symmetric(
                      horizontal: AppDimension.smallPadding),
                  child: Column(
                    children: List.generate(5, (index) {
                      if (index == 4) {
                        return Container(
                            margin: const EdgeInsets.only(
                                bottom: AppDimension.appSpaceVertical),
                            child: ItemTransaction(
                              onTap: () {
                                Get.to(()=>TransactionDetailPage());
                              },
                              onTapMenu: () {},
                            ));
                      }
                      return ItemTransaction(
                        onTap: () {
                          Get.to(()=>TransactionDetailPage());
                        },
                        onTapMenu: () {},
                      );
                    }),
                  ),
                ),

                const SizedBox(
                  height: 40,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
