import 'package:flutter/material.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';

class ItemDocumentWidget extends StatefulWidget {

  final GestureTapCallback onTap;
  final GestureTapCallback onTapMenu;

  ItemDocumentWidget(
  {
    required this.onTap,
    required this.onTapMenu
 }
 );

  @override
  State<ItemDocumentWidget> createState() => _ItemDocumentWidgetState();
}

class _ItemDocumentWidgetState extends State<ItemDocumentWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        margin: const EdgeInsets.only(bottom: AppDimension.appSpaceVertical,),
        padding: const EdgeInsets.all(AppDimension.appSpaceVertical),
        decoration: BoxDecoration(
          color: AppColor.whiteColor,
          borderRadius: BorderRadius.circular(AppDimension.defaultRadius)
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(Icons.picture_as_pdf_outlined,color: AppColor.redColor,size: AppDimension.getSizeIcon(context)*1.5,),

            const SizedBox(width: AppDimension.appSpaceVertical,),

            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Note.pdf",style: AppFont.textStyleHeader(fontWeight: FontWeight.w500,fontSize: AppDimension.getSizeSubTextTitleAddPropertyPage(context)-2),),
                  const SizedBox(height: AppDimension.appSpaceVertical/2.5,),
                  Text("1 week ago",style: AppFont.textStyleSubTitle(color: AppColor.greyColor,fontSize: AppDimension.getTextSizeTimeItemNotification(context)),)
                ],
              ),
            ),

            const Spacer(),

            InkWell(
                onTap: widget.onTapMenu,
                child: Icon(Icons.more_vert_outlined,color: AppColor.primaryColor,)
            )
          ],
        ),
      ),
    );
  }
}
