import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';

class SkeletonContainerWidget extends StatelessWidget {

 final List<Color> colors;

 final double height;

 final double width;

 final Color colorsContainer;

 SkeletonContainerWidget(
    {
     this.colors = const [
       Colors.black12,
       Colors.black26,
       Colors.black38,
     ],

     this.height = 10,
     this.width  = 100,
     this.colorsContainer = Colors.white
    }
);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      margin: const EdgeInsets.only(top: AppDimension.appSpaceVertical/2,bottom: AppDimension.appSpaceVertical/2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppDimension.defaultRadius),
        color: colorsContainer
      ),
    ).animate(
        onComplete: (AnimationController animationController){
          animationController.repeat();
        }
    ).shimmer(
        colors: colors,
        duration: const Duration(milliseconds: 1500)
    );
  }
}
