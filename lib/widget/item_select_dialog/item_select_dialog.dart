import 'package:flutter/material.dart';
import 'package:propertymanagement/app_style/app_color.dart';
import 'package:propertymanagement/app_style/app_dimension.dart';
import 'package:propertymanagement/app_style/app_font.dart';

class ItemSelectDialogWidget extends StatelessWidget {


  final String title;
  final bool isSelected;
  ItemSelectDialogWidget(
  {
    required this.title,
    required this.isSelected
   }
   );

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.all(4),
          margin: const EdgeInsets.symmetric(vertical: AppDimension.appSpaceVertical),
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle
          ),
          child: Icon(
            Icons.circle,
            size: MediaQuery.of(context).size.width>720?32:20,
            color: isSelected?AppColor.primaryColor:AppColor.whiteColor,
          ),
        ),

        const SizedBox(width: AppDimension.appSpaceVertical,),

        Text(title,style: AppFont.textStyleHeader(fontWeight: FontWeight.w400,fontSize: AppDimension.getSizeTextTitleItemProperty(context)+4),)
      ],
    );
  }
}
