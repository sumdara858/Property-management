import 'dart:io';

import 'package:image_picker/image_picker.dart';

class SelectFileHelper{


 static Future<File?> getImage({required ImageSource imageSource})async{

    XFile? pickedFile = await ImagePicker()
        .pickImage(source: imageSource);

    if(pickedFile!=null){
      return File(pickedFile.path);
    }


    return null;
  }

}