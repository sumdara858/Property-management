import 'package:get/get.dart';
import 'package:intl/intl.dart';

class StringFormat{


  static String formatCurrency(dynamic number){

    var currency = NumberFormat.simpleCurrency(name: "USD", decimalDigits: 1);

    if (number >= 1000) {
      currency = NumberFormat.simpleCurrency(name: "USD", decimalDigits: 3);

      String x = "${currency.format(number / 1000)}K";

      if (x.endsWith("00K")) x = x.substring(0, x.length - 3);

      x += "K";

      if (x.endsWith("KK")) x = x.substring(0, x.length - 1);

      return x;
    }
    

    if (number >= 1000000) {
      currency = NumberFormat.simpleCurrency(name: "USD", decimalDigits: 6);
      return "${currency.format(number / 1000000)}M";
    }
    
    return currency.format(number);


  }

  static String? validateEmail(String? value){

    if(value!.isEmpty){
      return "Email is required".tr;
    }

    if(!RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value.toString())){

      return "Invalid email".tr;
    }

    return null;
  }

  static String? validatePhoneNumber(String? value){

    if(value!.isEmpty){
      return "Phone number is required".tr;
    }

    if(value.toString().length>12||value.toString().length<9){
      return "Invalid phone number".tr;
    }

    return null;
  }


  static String? validatePassword(String? value){

    if(value!.isEmpty){
      return "Password is required".tr;
    }

    if(value.toString().length<8){
      return "Password at least 8 digits".tr;
    }


    return null;
  }


  static String? validateConfirmPassword(String? password,String? confirmPassword){

    if(confirmPassword!.isEmpty){
      return "Confirm Password is required".tr;
    }

    if(confirmPassword.toString()!=password){
      return "Confirm Password and Password not match".tr;
    }

    return null;
  }


  static String? validateForm(String? value){

    if(value!.isEmpty){
      return "Required*".tr;
    }


    return null;
  }



}